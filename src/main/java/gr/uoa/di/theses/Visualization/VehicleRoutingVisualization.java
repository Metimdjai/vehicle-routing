package gr.uoa.di.theses.Visualization;

import com.google.common.io.Resources;
import gr.uoa.di.theses.pathfinding.PathFinding;
import gr.uoa.di.theses.vr.GraphNode;
import gr.uoa.di.theses.vr.Vehicle;
import gr.uoa.di.theses.vr.VehicleRouting;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import org.apache.http.client.utils.URIBuilder;
import redis.clients.jedis.Jedis;

import javax.swing.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class VehicleRoutingVisualization{

    private static BufferedWriter bw;//writer parameter to kml file (in order to pass as static final to runnable threads)

    //caches for various query types

    private static Cache geoms;
    private static Cache roadData;
    private static Cache successors;
    private static Cache intersections;
    private static Cache roadWeights;

    //jedis database
    private static Jedis jedis;

    public static BufferedWriter getBw(){//get writer
        return bw;
    }

    public static void setBw(BufferedWriter bw_){//set writer
        bw=bw_;
    }

    //accessors to caches

    public static Cache getGeoms(){
        return geoms;
    }

    public static Cache getRoadData(){
        return roadData;
    }

    public static Cache getSuccessors(){
        return successors;
    }

    public static Cache getIntersections(){
        return intersections;
    }

    public static Cache getRoadWeights(){
        return roadWeights;
    }


    //accessor to jedis database
    public static Jedis getJedis(){
        return jedis;
    }

    public static void main(String[] args) {

        VehicleRouting vrInstance=new VehicleRouting();
        LinkedList<Vehicle> fleet=vrInstance.getFleet();

        jedis = new Jedis("localhost");

        //jedis.flushDB();

        //String to output to a kml file

        String kmlFile="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<kml xmlns=\"http://www.opengis.net/kml/2.2\">\n" +
                "<Document>\n" +
                "<Style id=\"truck\">\n" +
                "<IconStyle>\n" +
                "   <scale>1.75</scale>"+
                "  <Icon>\n" +
                String.format("    <href>%s</href>\n",new File(Resources.getResource("truck.png").getFile()).getAbsolutePath()) +
                "  </Icon>\n" +
                "</IconStyle>\n" +
                "</Style>\n" +
                "<Style id=\"depot\">\n" +
                "<IconStyle>\n" +
                "   <scale>1.75</scale>"+
                "  <Icon>\n" +
                String.format("    <href>%s</href>\n",new File(Resources.getResource("depot.png").getFile()).getAbsolutePath()) +
                "  </Icon>\n" +
                "</IconStyle>\n" +
                "</Style>";

        //choose a location and name file via gui user interaction to save the kml file
        JFileChooser c= new JFileChooser();
        c.showOpenDialog(c);
        File write_file = c.getSelectedFile();
        FileWriter fw=null;
        try
        {
            fw = new FileWriter(write_file);
            BufferedWriter bw = new BufferedWriter(fw);
            setBw(bw);
            bw.append(kmlFile);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        //initialize caches (to be passed to A* threads)
        CacheManager cm;

        cm = CacheManager.getInstance();
        cm.addCacheIfAbsent("geoms");
        cm.addCacheIfAbsent("roadData");
        cm.addCacheIfAbsent("successors");
        cm.addCacheIfAbsent("intersections");
        cm.addCacheIfAbsent("roadWeights");

        geoms=cm.getCache("geoms");
        roadData=cm.getCache("roadData");
        successors=cm.getCache("successors");
        intersections=cm.getCache("intersections");
        roadWeights=cm.getCache("roadWeights");

        /*
        geoms.removeAll();
        roadData.removeAll();
        successors.removeAll();
        intersections.removeAll();
        roadWeights.removeAll();
        */

        final long startTime = System.currentTimeMillis();

        //initialize thread pool
        ExecutorService threadExecutor = Executors.newCachedThreadPool();

        //for every vehicle in fleet
        for(Vehicle v:fleet){
            //for every pair of customers (depot included)

            //generate random 6-digit hex colour for route
            String[] letters;
            letters = "0123456789ABCDEF".split("");
            String colour ="#";
            for(int i=0;i<6;i++) {
                Double ind = Math.random() * 15;
                int index = (int)Math.round(ind);
                colour += letters[index];
            }
            //finally, add the two digits for transparency in kml format
            colour+="99";

            for(int i=0;i<v.getCustomers().size();i++){
                GraphNode c1,c2;
                //get customer nodes
                c1=v.getCustomers().get(i);
                c2=v.getCustomers().get((i + 1) % v.getCustomers().size());

                //initialize an A* search in order to get the shortest route from c1 to c2 in a new thread

                //pass to each thread the colour for their part of route, plus the type of the source placemark (truck's delivery spot or depot)
                PathFinding Astar=new PathFinding(c1.getCoordinates(),c2.getCoordinates(),colour,i==0?"depot":"truck");
                //run A* on a new thread
                threadExecutor.execute(Astar);
            }
        }

        //shutdown thread pool
        threadExecutor.shutdown();
        //and wait for threads to complete their tasks
        //max value is used to make sure threads have a long time to complete their tasks
        try {
            threadExecutor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //write final part to kml file
        kmlFile=("</Document>\n" +
                "</kml>");

        try {
            bw.append(kmlFile);
            assert fw!=null;
            //close file
            bw.close();
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        cm.shutdown();

        //using the user-specified path, generate a correct visualization html file to view the fully interactive windowed map in a window
        URL kml=null;
        try {
            kml = new URIBuilder(write_file.toURI()).build().toURL();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        assert(kml!=null);

        File visualization;
        visualization=new File(kml.getFile().substring(0, write_file.getAbsolutePath().lastIndexOf('/')),"visualization.html");

        try {
            if (!visualization.isFile() && !visualization.createNewFile())
            {
                throw new IOException("Error creating new file: " + visualization.getAbsolutePath());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        //simply write a space to the file to ensure its existence/overwriting
        try {
            BufferedWriter w = new BufferedWriter(new FileWriter(visualization));
            w.write(" ");
            w.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(String.format("\nKML file with the coordinates saved at %s .",write_file.getAbsolutePath()));
        System.out.println(String.format("HTML file with the visualization of the solution saved at %s .",visualization.getAbsolutePath()));
        System.out.println("Every customer will be displayed by a truck icon and the depot with a house icon.");
        System.out.println("KML file can be viewed in your web browser and/or any application/website capable of handling kml files like Google Earth and Marble.");

        try
        {

            visualization=new File(kml.getFile().substring(0,write_file.getAbsolutePath().lastIndexOf('/'))+"/visualization.html");
            FileWriter fw2 = new FileWriter(visualization);
            BufferedWriter bw2 = new BufferedWriter(fw2);

            //this is the actual content of the html file to write to

            bw2.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n" +
                    "<html>\n" +
                    " \n" +
                    " <head>\n" +
                    "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n" +
                    "    <title>VRPTW Visualization</title>\n" +
                    "<script type=\"text/javascript\" src=\"http://maps.google.com/maps/api/js?sensor=false\"></script>\n" +
                    "<style type=\"text/css\">\n" +
                    "     html { height: 100% }\n" +
                    "body { height: 100%; margin: 0; padding: 0 }\n" +
                    "#map_canvas{\n" +
                    "\twidth: 100%;\n" +
                    "\theight: 100%;\n" +
                    "}\n" +
                    "</style>\n" +
                    " \n" +
                    "<script src=\"http://code.jquery.com/jquery-1.10.1.min.js\"></script>\n" +
                    "<script src=\"https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=geometry\"></script>\n" +
                    "<script src=\"http://geoxml3.googlecode.com/svn/branches/polys/geoxml3.js\"></script>\n" +
                    "<script src=\"http://geoxml3.googlecode.com/svn/trunk/ProjectedOverlay.js\"></script>\n" +
                    " \n" +
                    "<script type=\"text/javascript\">\n" +
                    "\t\n" +
                    "        function initialize() {\n" +
                    "\t\n" +
                    "            var options = {\n" +
                    "                center: new google.maps.LatLng(-34.397, 150.644),\n" +
                    "                mapTypeId: google.maps.MapTypeId.ROADMAP,\n" +
                    "                                draggable: true,\n" +
                    "panControl:true,\n" +
                    "zoomControl:true,\n" +
                    "zoomControlOptions:{\n" +
                    "\tstyle:google.maps.ZoomControlStyle.LARGE,\n" +
                    "\tposition:google.maps.ControlPosition.LEFT_TOP\n" +
                    "},\n" +
                    "mapTypeControl:true,\n" +
                    "mapTypeControlOptions: {\n" +
                    "        style: google.maps.MapTypeControlStyle.DEFAULT\n" +
                    "    },\n" +
                    "scaleControl:true,\n" +
                    "streetViewControl:true,\n" +
                    "overviewMapControl:true,\n" +
                    "rotateControl:true,\n" +
                    "scrollwheel:true\n" +
                    "            };\n" +
                    "\t\t    \n" +
                    "            var map = new google.maps.Map(document.getElementById(\"map_canvas\"), options);\n" +
                    "            var parser = new geoXML3.parser({map: map, processStyles: true});\n");
            bw2.append(String.format("            parser.parse(\"%s\");\n", kml.getPath()));
            bw2.append(
                    "            \n" +
                            "            var worldBounds = new google.maps.LatLngBounds(new google.maps.LatLng(-85,-180),\n" +
                            "                                               new google.maps.LatLng(85,180));\n" +
                            "\t\t\tmap.fitBounds(worldBounds);\n" +
                            "\n" +
                            "\n" +
                            "\n" +
                            "            if (window.attachEvent) { window.attachEvent(\"onresize\", function( ) {this.map.onResize( )} );\n" +
                            "             window.attachEvent(\"onload\", function( ) {this.map.onResize( )} );\n" +
                            "              } else if (window.addEventListener) {\n" +
                            "              \twindow.addEventListener(\"resize\", function( ) {this.map.onResize( )\n" +
                            "              } , false);\n" +
                            "               window.addEventListener(\"load\", function( ) {this.map.onResize( )} , false);\n" +
                            "                }\n" +
                            "             \n" +
                            "\n" +
                            "\n" +
                            "        }\n" +
                            "\t    \n" +
                            "\t      google.maps.event.addDomListener(window, 'load', initialize);\n" +
                            "    </script>\n" +
                            "\t\n" +
                            "\t</head>\n" +
                            "<body onload=\"initialize()\">\n" +
                            "<div id=\"map_canvas\"></div>\n" +
                            "</body>\n" +
                            "\t\t\n" +
                            "</html>\n");
            bw2.close();
            fw2.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        final long endTime = System.currentTimeMillis();

        System.out.println("\nTotal execution time: " + (endTime - startTime) / 1000.0 + " seconds.");

    }
}
