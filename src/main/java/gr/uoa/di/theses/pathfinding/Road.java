package gr.uoa.di.theses.pathfinding;

import edu.uci.ics.jung.graph.util.Pair;

import java.util.LinkedList;

public class Road{
    private String name;//name of road
    private String oneway;//oneway status
    private LinkedList<Pair<Double>> geom;//geometry of road
    private String gid;//gid of road

    //constructor
    public Road(String name,String oneway,LinkedList<Pair<Double>> geom,String gid){
        this.name=name;
        this.oneway=oneway;
        this.geom=new LinkedList<Pair<Double>>(geom);
        this.gid=gid;
    }

    //accessors

    //returns name
    public String getName(){
        return name;
    }

    //returns oneway status
    public String isOneway(){
        return oneway;
    }

    //returns geometry
    public LinkedList<Pair<Double>> getGeom(){
        return geom;
    }

    //returns gid
    public String getGid(){
        return gid;
    }

    //output
    @Override
    public String toString(){
        return gid+" "+name+" "+oneway+" "+geom+" ";
    }
}
