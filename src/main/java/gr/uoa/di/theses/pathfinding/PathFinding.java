package gr.uoa.di.theses.pathfinding;

import java.io.*;
import java.util.*;

import edu.uci.ics.jung.graph.util.Pair;
import aima.core.agent.Action;
import aima.core.search.framework.GraphSearch;
import aima.core.search.framework.Problem;
import aima.core.search.framework.Search;
import aima.core.search.framework.SearchAgent;
import aima.core.search.informed.AStarSearch;
import gr.uoa.di.theses.Visualization.VehicleRoutingVisualization;
import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;
import redis.clients.jedis.Jedis;

public class PathFinding implements Runnable{

    private Pair<Double> source;//source point for A*
    private Pair<Double> target;//target point for A*
    private static final BufferedWriter bw;//writer to file for all threads
    //various caches for A* threads to use
    private static final Cache geoms;//cache with geometries of roads
    private static final Cache roadData;//cache with road's name and oneway status
    private static final Cache successors;//cache with successors of a road (all possible, without considering invalids)
    private static final Cache intersections;//cache with intersection points for two gids
    private static final Cache roadWeights;//cache with weights of roads
    private static final Jedis jedis;//interface with redis database (in localhost)
    private String colour;//colour of route
    private String sourceType;//to determine type of source icon (truck or depot)

    static{
        bw= VehicleRoutingVisualization.getBw();//initialize buffered writer
        //and the various caches
        geoms=VehicleRoutingVisualization.getGeoms();
        roadData=VehicleRoutingVisualization.getRoadData();
        successors=VehicleRoutingVisualization.getSuccessors();
        intersections=VehicleRoutingVisualization.getIntersections();
        roadWeights=VehicleRoutingVisualization.getRoadWeights();
        jedis=VehicleRoutingVisualization.getJedis();
    }

    //constructor
    public PathFinding(Pair<Double> source,Pair<Double> target,String colour,String sourceType){
        this.source=source;
        this.target=target;
        this.colour=colour;
        this.sourceType=sourceType;
    }

    //simply print stats of A* execution
    //synchronized exists to ensure threads will print their status uninterrupted
    private synchronized static void printInstrumentation(Properties properties) {

        //prints A* stats
        for(Object key:properties.keySet()){
            String property = properties.getProperty(key.toString());
            System.out.println(key + " : " + property);
        }
    }

    //prints actions from initial state to goal state for A*
    //again, synchronized exists to safely handle thread concurrency
    private synchronized static void printActions(List<Action> actions) {
        for(Action action:actions) {
            //prints roads that compose the solution

            Pair<String> roadData=roadDataContains(action.toString().replace("Action[name==", "").replace("]", ""));
            System.out.println(roadData.getFirst()+" "+action.toString().replace("Action[name==", "").replace("]", ""));
        }
    }

    //using the actions returned by A*, generate a list of points that composes the actual route
    private synchronized static LinkedList<Pair<Double>> generateRoute(List<Action> actions, PathFindingBoard state,Pair<Double> origSource){
        LinkedList<Pair<Double>> route=new LinkedList<Pair<Double>>();

        Pair<Double> firstPointOnRoad=origSource;

        //loop for every consecutive pair of roads (-1 exists in order to include a transition from the initial road to the first solution road)
        for(int i=-1;i<actions.size();i++) {
            String gid1, gid2;
            if (i == -1)
                gid1 = state.getSource().getGid();
            else
                gid1 = actions.get(i).toString().replace("Action[name==", "").replace("]", "");
            if (i != actions.size() - 1)
                gid2 = actions.get(i + 1).toString().replace("Action[name==", "").replace("]", "");
            else
                gid2 = state.getTarget().getGid();

            boolean entered = false;
            boolean exited = false;

            //obtain intersection point of the two consecutive roads

            Pair<Double> lastPointOnRoad;

            //if we reached final action, the next road of intersect is the target road, otherwise simply find the intersection of the two roads
            if (i != actions.size() - 1) {
                lastPointOnRoad = state.findIntersectionPoint(gid1, gid2);
            } else {
                //fix to getting target point
                lastPointOnRoad = state.getTargetPoint();
            }

            LinkedList<Pair<Double>> cands = state.getRoadGeom(gid1);

            //tmpPoints is a temporary list with the points to append to final route

            LinkedList<Pair<Double>> tmpPoints = new LinkedList<Pair<Double>>();

            //try collecting points starting from firstPointOnRoad up to lastPointOnRoad in increasing succession

            for (Pair<Double> somePointOnRoad : cands) {

                if (entered) {//first point found
                    tmpPoints.add(somePointOnRoad);//append next point
                    if (somePointOnRoad.equals(lastPointOnRoad)) {//if last point was found, all points from this road are collected
                        exited = true;
                        break;
                    }
                }

                if (somePointOnRoad.equals(firstPointOnRoad)) {
                    tmpPoints.add(firstPointOnRoad);
                    entered = true;
                    if (firstPointOnRoad.equals(lastPointOnRoad)) {//first points is the last as well
                        exited = true;
                        break;
                    }
                }
            }

            //if first and last point exist in reverse (the road was bidirectional)

            if (!(entered && exited)) {
                entered = false;
                exited = false;
                tmpPoints.clear();

                //again iterate on points,start collecting from lastPointOnRoad up to firstPointOnRoad this time
                for (Pair<Double> somePointOnRoad : cands) {

                    if (entered) {
                        tmpPoints.add(somePointOnRoad);
                        if (somePointOnRoad.equals(firstPointOnRoad)) {
                            exited = true;
                            break;
                        }
                    }

                    if (somePointOnRoad.equals(lastPointOnRoad)) {
                        tmpPoints.add(somePointOnRoad);
                        entered = true;
                        if (somePointOnRoad.equals(firstPointOnRoad)) {
                            exited = true;
                            break;
                        }
                    }
                }

                //now,reverse the list of points so that it is correctly appended to route
                cands.clear();
                for (Pair<Double> element : tmpPoints) {
                    cands.addFirst(element);
                }
                tmpPoints = cands;
            }

            if (exited) {
                //finally,append tmpPoints to final route if they were successfully collected

                for (Pair<Double> element : tmpPoints)
                    route.add(element);

            }

            //firstPointOnRoad for next loop becomes this loop's last
            firstPointOnRoad=lastPointOnRoad;
        }

        //append final point
        route.add(firstPointOnRoad);

        return route;
    }

    public void run(){

        //source and target points for A*

        Pair<Double> source=this.source;
        Pair<Double> target=this.target;

        //new instance of PathFinding class for A* execution

        PathFindingBoard routeFinder=new PathFindingBoard(source,target);

        //list of points that form the route

        LinkedList<Pair<Double>> route=new LinkedList<Pair<Double>>();

        try {
            //initiate A* search

        	Problem problem = new Problem(routeFinder, PathFindingFunctionFactory.getActionsFunction(), PathFindingFunctionFactory.getResultFunction(), new PathFindingGoalTest(), new PathFindingStepCostFunction());
        	Search search = new AStarSearch(new GraphSearch(),
					new DistanceHeuristicFunction());
            SearchAgent agent = new SearchAgent(problem, search);

            //print stats of A* execution and names of roads of the solution (for debugging purposes)

            printActions(agent.getActions());
            printInstrumentation(agent.getInstrumentation());

            //construct route from gids of collected roads from A*

            route=generateRoute(agent.getActions(),routeFinder,routeFinder.getSourcePoint());
        } catch (Exception e) {
            e.printStackTrace();
        }

        //all went well,close postgresql database

        routeFinder.closeCon();

        //ensure each thread writes its part to the kml file atomically

        synchronized(bw) {
            String kmlFile=" ";
            kmlFile+="<Placemark>\n";
            kmlFile+=String.format("        <name>%s</name>\n", routeFinder.getSource().getName());
            kmlFile+=String.format("<styleUrl>#%s</styleUrl>",sourceType);
            kmlFile+="        <Point>\n";
            kmlFile+=String.format(Locale.ENGLISH, "        <coordinates>%.10f,%.10f,0</coordinates>\n",routeFinder.getSourcePoint().getFirst(),routeFinder.getSourcePoint().getSecond());
            kmlFile+="        </Point>\n" +
                    "        </Placemark>";

            kmlFile+="<Placemark>\n"+
                    "<Style>\n" +
                    "<LineStyle>\n" +
                    "        <width>7</width>\n" +
                    String.format("        <color>%s</color>\n",colour) +
                    "    </LineStyle>\n" +
                    "</Style>" +
                    "    <LineString>\n" +
                    "        <coordinates>\n";

            //add points of route coordinates to kml file
            for(Pair<Double> point:route){
                kmlFile+=String.format(Locale.ENGLISH,"        %.10f,%.10f,0\n",point.getFirst(),point.getSecond());
            }

            kmlFile+=("        </coordinates>\n" +
                    "    </LineString>\n" +
                    "</Placemark>\n");


            try {
                bw.append(kmlFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("unchecked")
    public synchronized static LinkedList<Pair<Double>> geomsContains(String gid){
        //if the key exists, return value,else return null
        Element elem=geoms.get(gid);
        return elem==null?null:(LinkedList<Pair<Double>>)elem.getObjectValue();
    }

    @SuppressWarnings("unchecked")
    public synchronized static Pair<String> roadDataContains(String gid){
        //if the key exists, return value,else return null
        Element elem=roadData.get(gid);
        return elem==null?null:(Pair<String>)elem.getObjectValue();
    }

    @SuppressWarnings("unchecked")
    public synchronized static LinkedList<Successor> successorsContains(String gid){
        //if the key exists, return value,else return null
        Element elem=successors.get(gid);
        return elem==null?null:(LinkedList<Successor>)elem.getObjectValue();
    }

    @SuppressWarnings("unchecked")
    public synchronized static Pair<Double> intersectionsContains(String gid1,String gid2) {
        //if the key exists, return value,else return null
        Element elem = intersections.get(gid1 + gid2);
        return elem == null ? null : (Pair<Double>) elem.getObjectValue();
    }

    public synchronized static Double roadWeightsContains(String gid) {
        //if the key exists, return value,else return null
        Element elem = roadWeights.get(gid);
        return elem == null ? null:(Double) elem.getObjectValue();
    }

    public synchronized static String jedisContains(String gid){
        //if the key exists, return value, else return null;
        return jedis.exists(gid)?jedis.get(gid):null;
    }

    public synchronized static void addToGeoms(String gid,LinkedList<Pair<Double>> geom){
        //add key-value pair to cache if absent
        geoms.putIfAbsent(new Element(gid, new LinkedList<Pair<Double>>(geom)));
    }

    public synchronized static void addToRoadData(String gid,String name,String oneway){
        //add key-value pair to cache if absent
        roadData.putIfAbsent(new Element(gid,new Pair<String>(name,oneway)));
    }

    public synchronized static void addToSuccessors(String gid,LinkedList<Successor> data){
        //add key-value pair to cache if absent
        successors.putIfAbsent(new Element(gid,new LinkedList<Successor>(data)));
    }

    public synchronized static void addToIntersections(String gid1,String gid2,Pair<Double> intersection){
        //add key-value pair to cache absent
        intersections.putIfAbsent(new Element(gid1+gid2, new Pair<Double>(intersection)));
    }

    public synchronized static void addToRoadWeights(String gid,Double weight){
        //add key-value pair to cache absent

        //if data already in cache
        if(roadWeightsContains(gid)!=null) {
            //remove old value of weight
            roadWeights.remove(gid);
            //and add the new one
            roadWeights.putIfAbsent(new Element(gid,weight));
        }
        else{
            //no previous value in cache, simply add the new gid-weight pair
            roadWeights.putIfAbsent(new Element(gid,weight));
        }
    }
}
