package gr.uoa.di.theses.pathfinding;

import aima.core.search.framework.HeuristicFunction;

public class DistanceHeuristicFunction implements HeuristicFunction{

    //return distance of current point of state from the target
    public double h(Object o) {
        PathFindingBoard curPos=(PathFindingBoard) o;

        Double weight;

        //look for the road's weight in cache
        weight=PathFinding.roadWeightsContains(curPos.getSource().getGid());

        if(weight==null){//if not in cache
            //look it up in the database
            String res=PathFinding.jedisContains(curPos.getSource().getGid());
            if(res!=null)//if the returned value is not an empty string (and therefore can be parsed to Double)
                weight=Double.parseDouble(res);
        }

        if(weight==null){//if no traffic info available in redis database for this road/gid, add to cache a temporary value of 0.0 (no traffic and no bias of choosing this road)
            weight=0.0;
            PathFinding.addToRoadWeights(curPos.getSource().getGid(), weight);
        }

        return 0.5*curPos.getDist(curPos.getSourcePoint(),curPos.getTargetPoint());   //heuristic for A* return distance of current position from target in earth coordinates
    }
}
