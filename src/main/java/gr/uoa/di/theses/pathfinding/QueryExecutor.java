package gr.uoa.di.theses.pathfinding;

import java.sql.*;

public class QueryExecutor {

    private Connection connection;//connection to postgresql

    //constructor
    public QueryExecutor(){
        //try connecting to postgresql and constructing cache manager and caches
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        this.connection = null;
        try {
            this.connection = DriverManager.getConnection(
                    "jdbc:postgresql://ariadni.di.uoa.gr:5432/hellas", "iosif",
                    "i0s1f");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //returns query result
    public ResultSet getResult(String query){
        //use connection to execute query and obtain its result
        Statement test;
        ResultSet result=null;
        try {
            test = connection.createStatement();
            result=test.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    //closes running connection
    public void closeCon(){
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
