package gr.uoa.di.theses.pathfinding;

import edu.uci.ics.jung.graph.util.Pair;

import java.io.*;

public class Successor implements Serializable{

    private static final long serialVersionUID = 2026472609490944705L;//UID for serialization of class in cache

    private String gid;//gid of successor
    private Pair<Double> cand;//point that successor intersects with current road


    //constructor
    public Successor(String gid,Pair<Double> cand){
        this.gid=gid;
        this.cand=new Pair<Double>(cand);
    }

    //accessors

    //returns gid of successor
    public String getGid(){
        return gid;
    }

    //returns successsor's point
    public Pair<Double> getCand(){
        return  cand;
    }

    //output
    @Override
    public String toString(){
        return gid+" "+cand;
    }
}
