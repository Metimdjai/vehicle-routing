package gr.uoa.di.theses.pathfinding;

import aima.core.agent.Action;
import edu.uci.ics.jung.graph.util.Pair;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PathFindingBoard {

    private Road source;//current road of state
    private Road target;//target road for state
    private Pair<Double> origSource;//point of initial state
    private Pair<Double> sourcePoint;//current point of state
    private Pair<Double> targetPoint;//target point for state
    private QueryExecutor query;//class to get SQL queries results

    //constructor
    public PathFindingBoard(Pair<Double> source,Pair<Double> target) {
        //initialize query executor
        this.query=new QueryExecutor();

        //initialize the points

        //values are set to null as the points will be initialized when gathering info for the initial and goal roads
        //in case they do not exist in the roads' geometries, they will be properly snapped to their nearest

        this.origSource=null;
        this.sourcePoint=null;
        this.targetPoint=null;

        //get source and target roads
        this.source=getRoadInfo(source);
        this.target=getRoadInfo(target);
    }

    //copy constructor
    public PathFindingBoard(PathFindingBoard oldPos){
        query=oldPos.query;

        source=oldPos.source;
        target=oldPos.target;

        origSource=oldPos.origSource;
        sourcePoint=oldPos.sourcePoint;
        targetPoint=oldPos.targetPoint;
    }

    //returns the geometry of the road with gid parameter
    public LinkedList<Pair<Double>> getRoadGeom(String gid){

        LinkedList<Pair<Double>> geom=PathFinding.geomsContains(gid);

        //if geometry already in cache, return it
        if(geom!=null)
            return geom;

        //otherwise, query our database to obtain it

        geom=new LinkedList<Pair<Double>>();

        ResultSet rs = getQueryResult(String.format("SELECT ST_AsText(geom) as result FROM (SELECT (ST_DumpPoints(g.geom)).* FROM roads AS g WHERE gid=%d ) j;", Integer.parseInt(gid)));
        Pattern p = Pattern.compile("([0-9]*\\.?[0-9]*) ([0-9]*\\.?[0-9]*)");

        try {
            while (rs.next()) {
                Matcher m = p.matcher(rs.getString("result").replace("POINT(", "").replace(")", ""));
                if (!m.find()) {
                    System.out.println("Pattern matching failed");
                }

                geom.add(new Pair<Double>(Double.parseDouble(m.group(1)), Double.parseDouble(m.group(2))));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        //finally, add the obtained geometry to cache for future reference
        PathFinding.addToGeoms(gid, geom);
        return geom;
    }

    public LinkedList<Successor> getSuccessors(){

        //uncomment this for debug

        //System.out.println("@" + getSource().getName() + " " + getSource().getGid() +" "+sourcePoint+" " +getSource().getGeom()+" "+source.isOneway());


        //try obtaining possible successors of current road (intersections with other roads)
        LinkedList<Successor> successors=PathFinding.successorsContains(getSource().getGid());

        //if they are not in cache, query database to obtain them
        if(successors==null) {//result not in cache, query database and add it for future reference

            successors = new LinkedList<Successor>();

            //the query will obtain all gids and points of intersections with other roads in orientation order (VERY important for oneway roads)
            ResultSet rs = getQueryResult(String.format("select \n" +
                    "ST_AsText((ST_DumpPoints(ST_Intersection(a.geom,b.geom))).geom) as result,\n" +
                    "b.gid as resultGid,\n" +
                    "b.name as resName,\n" +
                    "ST_LineLocatePoint(ST_LineMerge(a.geom),(ST_DumpPoints(ST_Intersection(a.geom,b.geom))).geom) as sorting\n" +
                    "                        \n" +
                    "from roads as a,roads as b\n" +
                    "where a.gid=%d \n" +
                    "and a.gid!=b.gid \n" +
                    "and ST_intersects(geomfromewkt(a.geom),b.geom) \n" +
                    "order by ST_LineLocatePoint(ST_LineMerge(a.geom),(ST_DumpPoints(ST_Intersection(a.geom,b.geom))).geom);", Integer.parseInt(source.getGid())));

            try {
                Pattern p = Pattern.compile("([0-9]*\\.?[0-9]*) ([0-9]*\\.?[0-9]*)");
                while (rs.next()) {
                    String gid = rs.getString("resultGid");
                    String pointString = rs.getString("result").replace("POINT(", "").replace(")", "");
                    Matcher m = p.matcher(pointString);
                    if (!m.find()) {
                        System.out.println("Pattern matching failed");
                    }

                    Pair<Double> cand = new Pair<Double>(Double.parseDouble(m.group(1)), Double.parseDouble(m.group(2)));

                    successors.add(new Successor(gid, cand));

                    //add our findings to cache as well for future reference
                    PathFinding.addToIntersections(gid, source.getGid(), cand);
                    PathFinding.addToIntersections(source.getGid(),gid,cand);
                }
                //add the entire list to the corresponding cache as well
                PathFinding.addToSuccessors(source.getGid(), successors);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        LinkedList<Successor> result=new LinkedList<Successor>(successors);
        LinkedList<Successor> invalidSuccessors=new LinkedList<Successor>();


        //if current road is oneway
        if(!source.isOneway().equals("1")){
            //if it is a general state (and not the initial one)
            if(!sourcePoint.equals(origSource)) {

                //remove successors iff the source point exists in successors
                boolean foundPoint=false;

                for(Successor successor:successors){
                    if(successor.getCand().equals(sourcePoint)){
                        foundPoint=true;
                        break;
                    }
                }

                if(foundPoint) {//our current location is a valid successor, therefore we remove any intersections from this position from possible next states
                    for (Successor successor : successors) {
                        if (successor.getCand().equals(sourcePoint)) {
                            invalidSuccessors.add(successor);
                        }
                    }

                    for (Successor invalidSuccessor : invalidSuccessors) {
                        result.remove(invalidSuccessor);
                    }
                }
                else {//our location was a random point in the road without intersections, programmatically we should never reach this case
                    //defensively, set next states from this one to none (empty list)
                    result = invalidSuccessors;
                }
            }
            //else case (which does not need any handling):first road, so we give the current position's successors a chance in case it can instantly move to another road
        }
        else{
            //oneway road handling

            if(!sourcePoint.equals(origSource)) {
                //general case oneway road (and not the initial one)

                boolean foundPoint=false;

                for(Successor successor:successors){
                    if(successor.getCand().equals(sourcePoint)){
                        foundPoint=true;
                        break;
                    }
                }

                if(foundPoint){//our current location is a possible area of successor intersections, so we must remove every candidate intersection of the road
                    //up to this point (this point included)

                    LinkedList<Pair<Double>> geom=source.getGeom();


                    //try finding a next point so that we can add all points up to the current one (current one included) to list of invalid successors
                    //we are looking for a next point that belongs in the current road's geometry but also is an intersection point
                    boolean entered=false;
                    Pair<Double> nextPoint=null;

                    for(Pair<Double> point:geom){
                        if(entered){
                            for(Successor successor:successors) {
                                if(successor.getCand().equals(point)) {
                                    nextPoint = point;
                                    break;
                                }
                            }
                        }
                        if(nextPoint!=null)
                            break;
                        if(point.equals(sourcePoint)){
                            entered=true;
                        }
                    }

                    //there is no next point, so we are forced to allow this point's successors as next state candidates
                    //the previous points' candidates will be discarded as usual though
                    if(nextPoint==null){
                        //next point becomes sourcePoint
                        nextPoint=sourcePoint;
                        for (Successor successor : successors) {
                            if (successor.getCand().equals(nextPoint)) {
                                break;
                            }
                            invalidSuccessors.add(successor);
                        }

                        for(Successor successor:invalidSuccessors) {
                            result.remove(successor);
                        }
                    }
                    else{//we found a valid next point, so we discard every point up to the current one (included)
                        for (Successor successor : successors) {
                            if (successor.getCand().equals(nextPoint)) {
                                break;
                            }
                            invalidSuccessors.add(successor);
                        }

                        for(Successor successor:invalidSuccessors) {
                            result.remove(successor);
                        }
                    }

                }
                else{
                    //our location was a random point in the road without intersections, programmatically we should never reach this case
                    //defensively, set next states from this one to none (empty list)
                    result = invalidSuccessors;
                }
            }
            else{
                //in this case, we handle the initial road which happens to be a oneway one

                boolean foundPoint=false;
                Pair<Double> nextPoint=null;

                //first of all, try to see if our location is an intersection point or a random geometry point
                for(Successor successor:successors){
                    if(successor.getCand().equals(sourcePoint)){
                        foundPoint=true;
                        break;
                    }
                }

                if(foundPoint){
                    //our current (and initial position) is an intersection point, therefore we must discard any previous successors, but not the successors of this position
                    //since it is an initial state and must be given more choices of movement

                    nextPoint=sourcePoint;
                    for (Successor successor : successors) {
                        if (successor.getCand().equals(nextPoint)) {
                            break;
                        }
                        invalidSuccessors.add(successor);
                    }

                    for(Successor successor:invalidSuccessors) {
                        result.remove(successor);
                    }
                }
                else{
                    //our current (and initial) position is a random geometry point
                    //therefore, we must discard all points up to the first intersection point in the geometry we encounter
                    // (and NOT discard that one because it is a valid candidate for next state)

                    LinkedList<Pair<Double>> geom=source.getGeom();

                    //iterate on the geometry of current road for one such point
                    //when the current position is found, a next point is a valid point iff it belongs to successors as well (successors list is list of intersection points and roads)
                    for(Pair<Double> point:geom){
                        if(foundPoint){
                            for(Successor successor:successors){
                                if(successor.getCand().equals(point)){
                                    nextPoint=point;
                                    break;
                                }
                            }
                        }
                        if(nextPoint!=null)
                            break;

                        if(point.equals(sourcePoint)){
                            foundPoint=true;
                        }
                    }

                    //we found the next point, so we discard every successor up to next point (NOT included, as it is a valid next state candidate)
                    if(nextPoint!=null){
                        for (Successor successor : successors) {
                            if (successor.getCand().equals(nextPoint)) {
                                break;
                            }
                            invalidSuccessors.add(successor);
                        }

                        for(Successor successor:invalidSuccessors) {
                            result.remove(successor);
                        }
                    }
                    else{
                        //our location was a random point in the road without intersections, programmatically we should never reach this case
                        //defensively, set next states from this one to none (empty list)
                        result=invalidSuccessors;
                    }

                }
            }
        }

        //uncomment this for debug

        /*
        for(Successor successor:result){
            String name=getRoadInfo(successor.getGid()).getName();
            System.out.println(name + " " + successor.toString()+" "+getRoadInfo(successor.getGid()).isOneway());
        }
        */

        //after filtering the successors for both oneway and bidirectional roads, try further discarding oneway roads
        //if their point of intersection is their final one (and therefore we wouldn't be able to go anywhere from there or we would end up on the same state)

        //also, in case one of the successors is the target road, DISCARD it as a candidate only if:
        //-it is oneway
        //-its target point cannot be reached by the current intersection since the target road is oneway

        LinkedList<Successor> furtherInvalids=new LinkedList<Successor>();

        for(Successor successor:result){
            String gid=successor.getGid();
            Road roadInfo=getRoadInfo(gid);

            //only discard more candidates if they are oneway
            if(roadInfo.isOneway().equals("1")) {

                //using their geom, discard them if they do not have a next point (their final point is the point of intersection with the current road)

                LinkedList<Pair<Double>> geom = roadInfo.getGeom();
                boolean entered=false;
                boolean foundNext=false;
                for(Pair<Double> point:geom){
                    if(entered){
                        foundNext=true;
                        break;
                    }
                    if(point.equals(successor.getCand())){
                        entered=true;
                    }
                }
                if(!foundNext)
                    furtherInvalids.add(successor);

                if(successor.getGid().equals(target.getGid())&&!furtherInvalids.contains(successor)){
                    //in case the successor is the target road and it has not already been discarded

                    //see if its target point can be reached from current state

                    if(!successor.getCand().equals(targetPoint)) {
                        foundNext = false;
                        entered = false;
                        for (Pair<Double> point : geom) {
                            if (entered && point.equals(targetPoint)) {
                                foundNext = true;
                                break;
                            }
                            if (point.equals(successor.getCand())) {
                                entered = true;
                            }
                        }
                        if (!foundNext)//target point unreachable from current state, therefore target must be discarded as well
                            furtherInvalids.add(successor);
                    }
                }
            }
        }

        //finally, remove any further invalids obtained
        for(Successor invalidSuccessor:furtherInvalids){
            result.remove(invalidSuccessor);
        }

        return result;
    }

    //returns result of the query parameter
    public ResultSet getQueryResult(String query){
        return this.query.getResult(query);
    }

    //returns current road
    public Road getSource(){
        return source;
    }

    //returns target road
    public Road getTarget() {
        return target;
    }

    //returns current point
    public Pair<Double> getSourcePoint(){
        return sourcePoint;
    }

    //returns target point
    public Pair<Double> getTargetPoint(){
        return targetPoint;
    }

    //close the database connection
    public void closeCon(){
        this.query.closeCon();
    }

    //obtain road info based on point of interest
    public Road getRoadInfo(Pair<Double> point){
        //this function will be actually called only from the constructor of the class to obtain the gids and road info for initial and goal roads, so we just query the database for those

        String name,gid;
        String oneway;
        name=gid=oneway=null;

        //find the name, gid and oneway status of road the point parameter is located on
        //the query is careful to choose alphabetically a gid, if two or more roads intersect on the parameter point and sort by gid in case a long road intersects the current one
        //and its two parts intersect at that exact spot

        //the query also snaps the point parameter to the closest within the actual linestring to ensure stability of execution
        String sqlQuery;
        sqlQuery = String.format(Locale.ENGLISH, "SELECT gid, name,oneway, st_distance(st_pointfromtext('POINT(' || %.10f || ' ' || %.10f || ')', 4326), geom) AS dist,\n" +
                "ST_AsText(ST_Snap(st_pointfromtext('POINT(' || %.10f || ' ' || %.10f || ')', 4326),geom,0.0001)) as snappedPoint\n"+
                "FROM roads\n" +
                "WHERE st_distance(st_pointfromtext('POINT(' || %.10f || ' ' || %.10f || ')', 4326), geom) < 0.002 ORDER BY dist,name,gid LIMIT 1;\n", point.getFirst(), point.getSecond(),point.getFirst(), point.getSecond(), point.getFirst(), point.getSecond());

        ResultSet rs = query.getResult(sqlQuery);

        try {
            while (rs.next()) {
                gid = rs.getString("gid");
                name = rs.getString("name");
                oneway = rs.getString("oneway");

                //initialize target point by snapping it to position iff the original point got initialized first
                if(origSource!=null&&targetPoint==null){
                    Matcher m = Pattern.compile("([0-9]*\\.?[0-9]*) ([0-9]*\\.?[0-9]*)").matcher(rs.getString("snappedPoint"));
                    if (!m.find()) {
                        System.out.println("Pattern matching failed");
                    }

                    targetPoint=new Pair<Double>(Double.parseDouble(m.group(1)), Double.parseDouble(m.group(2)));
                }

                //initialize the origSource point by snapping it to position iff it has not already been snapped (target point check exists for safety)
                if(targetPoint==null&&origSource==null){
                    Matcher m = Pattern.compile("([0-9]*\\.?[0-9]*) ([0-9]*\\.?[0-9]*)").matcher(rs.getString("snappedPoint"));
                    if (!m.find()) {
                        System.out.println("Pattern matching failed");
                    }

                    origSource=new Pair<Double>(Double.parseDouble(m.group(1)), Double.parseDouble(m.group(2)));
                    sourcePoint=new Pair<Double>(Double.parseDouble(m.group(1)), Double.parseDouble(m.group(2)));
                }
                if (name == null)
                    name = "null";
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        LinkedList<Pair<Double>> geom=getRoadGeom(gid);


        //add our finding to cache
        PathFinding.addToRoadData(gid,name,oneway);

        return new Road(name,oneway,geom,gid);
    }

    //returns road info from gid
    public Road getRoadInfo(String gid){

        String name;
        String oneway;

        Pair <String> roadData=PathFinding.roadDataContains(gid);

        //if info is not in cache, we must query the database
        if(roadData==null){
            String sqlQuery;
            //simply obtain various road data
            sqlQuery = String.format(Locale.ENGLISH, "SELECT name,oneway FROM roads WHERE gid=%d LIMIT 1 ;",Integer.parseInt(gid));

            ResultSet rs = query.getResult(sqlQuery);

            try {
                while (rs.next()) {
                    name = rs.getString("name");
                    oneway = rs.getString("oneway");
                    if (name == null)
                        name = "null";
                    roadData=new Pair<String>(name,oneway);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        assert roadData!=null;
        //add them to cache (will only be added if absent)
        PathFinding.addToRoadData(gid,roadData.getFirst(),roadData.getSecond());

        return new Road(roadData.getFirst(),roadData.getSecond(),getRoadGeom(gid),gid);
    }

    //finds intersection point for the two roads with corresponding gid parameters
    public Pair<Double> findIntersectionPoint(String gid1,String gid2){
        Pair<Double> intersection=PathFinding.intersectionsContains(gid1,gid2);

        //if intersection of gid1 with gid2 does not exist, try obtaining intersection of gid2 with gid1
        if(intersection==null)
            intersection=PathFinding.intersectionsContains(gid2,gid1);

        //if that failed as well, we must query the database
        if(intersection==null){
            //the query again is very careful to obtain only one intersection point for the two gids
            //(as two points would be problematic and in any case two or more intersections can be considered as one since they give the same state)
            ResultSet rs=getQueryResult(String.format("\n" +
                    "select \n" +
                    "ST_AsText((ST_DumpPoints(ST_Intersection(a.geom,b.geom))).geom) as result,\n" +
                    "b.gid as resultGid,\n" +
                    "b.name as resName,\n" +
                    "ST_LineLocatePoint(ST_LineMerge(a.geom),(ST_DumpPoints(ST_Intersection(a.geom,b.geom))).geom) as sorting\n" +
                    "from roads as a,roads as b\n" +
                    "where a.gid=%d\n" +
                    "and b.gid =%d\n" +
                    "and ST_intersects(geomfromewkt(a.geom),b.geom) \n" +
                    "order by ST_LineLocatePoint(ST_LineMerge(a.geom),(ST_DumpPoints(ST_Intersection(a.geom,b.geom))).geom) LIMIT 1;",Integer.parseInt(gid1),Integer.parseInt(gid2)));
            try {
                Pattern p = Pattern.compile("([0-9]*\\.?[0-9]*) ([0-9]*\\.?[0-9]*)");
                while (rs.next()) {

                    String sqlQuery = rs.getString("result");

                    Matcher m = p.matcher(sqlQuery);
                    if (!m.find()) {
                        System.out.println("Pattern matching failed");
                    }

                    intersection=new Pair<Double>(Double.parseDouble(m.group(1)), Double.parseDouble(m.group(2)));

                    //add our findings to cache for future reference
                    PathFinding.addToIntersections(gid1,gid2,intersection);
                    PathFinding.addToIntersections(gid2,gid1,intersection);
                }
            }catch(SQLException e) {
                e.printStackTrace();
            }
        }

        return intersection;
    }

    //computes distance of two points on earth
    public double getDist(Pair<Double> source,Pair<Double> target){

        Double lat1,lon1,lat2,lon2;

        lat1=source.getFirst();
        lon1=source.getSecond();
        lat2=target.getFirst();
        lon2=target.getSecond();

        Double R = 6371.0; // km
        Double dLat = (lat2-lat1)*(Math.PI/180.0);
        Double dLon = (lon2-lon1)*(Math.PI/180.0);
        Double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(lat1*(Math.PI/180.0)) * Math.cos(lat2*(Math.PI/180.0)) *
                        Math.sin(dLon/2) * Math.sin(dLon/2);
        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        return R * c;
    }

    @Override
    public boolean equals(Object o) {
        //equality of states holds if both states share the same current gid (are on the same road)

        if (this == o) {
            return true;
        }
        if ((o == null) || (this.getClass() != o.getClass())) {
            return false;
        }
        PathFindingBoard newPos = (PathFindingBoard) o;

        return newPos.source.getGid().equals(this.source.getGid());
    }

    @Override
    public int hashCode() {
        //hash code for A* is simply the current gid of the state
        return Integer.parseInt(this.source.getGid());
    }

    //destination is reached if current road is the target road
    public boolean reachedDestination(){
        return this.source.getGid().equals(this.target.getGid());
    }

    //applies change of state for A* execution
    public void applyChange(Action a) {
        sourcePoint=findIntersectionPoint(source.getGid(), a.toString().replace("Action[name==", "").replace("]", ""));
        source=getRoadInfo(a.toString().replace("Action[name==", "").replace("]", ""));
    }
}
