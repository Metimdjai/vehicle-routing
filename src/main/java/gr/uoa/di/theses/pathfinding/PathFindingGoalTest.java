package gr.uoa.di.theses.pathfinding;

import aima.core.search.framework.GoalTest;

public class PathFindingGoalTest implements GoalTest {
    //returns true iff we reached target point
    public boolean isGoalState(Object o) {
    	PathFindingBoard curPos=(PathFindingBoard) o;
        return curPos.reachedDestination();//we have reached our goal if we have reached the target point
    }
}
