package gr.uoa.di.theses.pathfinding;

import aima.core.agent.Action;
import aima.core.search.framework.StepCostFunction;

public class PathFindingStepCostFunction implements StepCostFunction{

	//returns cost from source to target
	public double c(Object s, Action a, Object sDelta) {
		Double weight;
		PathFindingBoard curPos=(PathFindingBoard) s;
		PathFindingBoard nextPos=(PathFindingBoard) sDelta;

		//look for the road's weight in cache
		weight=PathFinding.roadWeightsContains(curPos.getSource().getGid());

		if(weight==null){//if not in cache
			//look it up in the database
			String res=PathFinding.jedisContains(curPos.getSource().getGid());
			if(res!=null)//if the returned value is not an empty string (and therefore can be parsed to Double)
				weight=Double.parseDouble(res);
		}

		if(weight==null){//if no traffic info available in redis database for this road/gid, add to cache a temporary value of 0.0 (no traffic and no bias of choosing this road)
			weight=0.0;
			PathFinding.addToRoadWeights(curPos.getSource().getGid(),weight);
		}

		return (1.0+weight)*curPos.getDist(curPos.getSourcePoint(),nextPos.getSourcePoint())+0.000001;
	}

}
