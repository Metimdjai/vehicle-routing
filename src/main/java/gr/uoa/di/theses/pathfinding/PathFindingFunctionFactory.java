package gr.uoa.di.theses.pathfinding;

import aima.core.agent.Action;
import aima.core.agent.impl.DynamicAction;
import aima.core.search.framework.ActionsFunction;
import aima.core.search.framework.ResultFunction;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Set;

public class PathFindingFunctionFactory {
    private static ActionsFunction _actionsFunction = null;
    private static ResultFunction _resultFunction = null;

    //actions
    public static ActionsFunction getActionsFunction() {
        if (null == _actionsFunction) {
            _actionsFunction = new EPActionsFunction();
        }
        return _actionsFunction;
    }

    //application of actions
    public static ResultFunction getResultFunction() {
        if (null == _resultFunction) {
            _resultFunction = new EPResultFunction();
        }
        return _resultFunction;
    }

    //available actions from current state of A*
    private static class EPActionsFunction implements ActionsFunction {
        public Set<Action> actions(Object state) {
            PathFindingBoard curPos = (PathFindingBoard) state;

            Set<Action> actions = new LinkedHashSet<Action>();

            LinkedList<Successor> successors=curPos.getSuccessors();

            for(Successor successor:successors){
                actions.add(new DynamicAction(successor.getGid()));
            }

            return actions;

        }
    }

    //simply apply corresponding action to reach a new state
    private static class EPResultFunction implements ResultFunction {
        public Object result(Object s, Action a) {
            PathFindingBoard curPos = (PathFindingBoard) s;

            //ensure an actual state and action exist
            if(a!=null) {

                PathFindingBoard newPos = new PathFindingBoard(curPos);
                //apply action to state to obtain a new state
                newPos.applyChange(a);
                return newPos;
            }
            return s;
        }
    }

}
