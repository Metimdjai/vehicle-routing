package gr.uoa.di.theses.vr;

//import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;

import com.google.common.io.Resources;

public class VehicleRouting {

	//public static Logger LOGGER = Logger.getLogger(VehicleRouting.class);
    LinkedList<Vehicle> fleet;

    //public static void main(String[] args){

	public VehicleRouting(){

        File dir = new File(Resources.getResource("testCases").getFile());
        File[] directoryListing=dir.listFiles();

        if(directoryListing!=null){
            for (File child:directoryListing){

                FileOutputStream fout = null;
                try{
                    fout=new FileOutputStream(child.getAbsolutePath().substring(0,child.getAbsolutePath().lastIndexOf('/'))+"/../Output_"+child.getName());
                }catch (FileNotFoundException e){
                    e.printStackTrace();
                }

                System.out.println(child.getAbsolutePath().substring(0,child.getAbsolutePath().lastIndexOf('/'))+"/../Output_"+child.getName());

                byte[] b;

                Double lambda=0.2;

                VRPTWData instance=new VRPTWData(child.getAbsolutePath());//generate new vrptw instance

                instance.initialSolution();//get initial solution for vrptw

                b=instance.toString().getBytes();
                try {
                    assert fout!=null;
                     fout.write(b);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                GLS bestGLS=new GLS(instance.getFleet(),instance.getGraph(),lambda,instance.getCapacity());//generate new guided local search instance
                bestGLS.localSearch();//do a local search on it based on the 4 operators

                bestGLS.setSolution();//set best solution as the current one

                int maxIterations=2000;
                int iterations=maxIterations;

                boolean entered=false;

                LinkedList<Vehicle> oldFleet=null;

                while (iterations-->=0) {//try iterations improvement steps

                    bestGLS.penaliseVector();//based on the changes done to the solution, compute new penalty vector

                    b=bestGLS.localSearch().getBytes();//do a local search on it based on the 4 operators
                    try {
                        fout.write(b);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if(!entered){
                        entered=true;
                        oldFleet=new LinkedList<Vehicle>(bestGLS.getFleet());
                    }

                    if(bestGLS.solutionImproved()){//if the solution is better than current best
                        bestGLS.setSolution();//set best solution as the current one

                        b="Found a better solution!\n".getBytes();
                        try {
                            fout.write(b);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        b=bestGLS.toString().getBytes();
                        try {
                            fout.write(b);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    if(bestGLS.equalSolution(oldFleet)){
                        break;
                    }
                    else{
                        oldFleet=new LinkedList<Vehicle>(bestGLS.getFleet());
                    }
                }

                b="Number of steps of improvement in GLS: ".getBytes();
                try {
                    fout.write(b);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                b=Integer.valueOf(maxIterations-iterations).toString().getBytes();
                try {
                    fout.write(b);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                b="\n\nFINAL SOLUTION: \n".getBytes();
                try {
                    fout.write(b);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                b=bestGLS.toString().getBytes();
                try {
                    fout.write(b);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    fout.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                //when turned to simple class,uncomment next line

                this.fleet=bestGLS.getFleet();

            }
        }

	}

    public LinkedList<Vehicle> getFleet(){
        return this.fleet;
    }
}
