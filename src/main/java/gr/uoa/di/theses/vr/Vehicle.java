package gr.uoa.di.theses.vr;

import java.util.LinkedList;

public class Vehicle {

    //members of vehicle class

    private int capacity;
    private LinkedList<GraphNode> customers;
    private LinkedList<Double> waitingTimes;
    private LinkedList<Double> arrivalTimes;

    //constructor
    public Vehicle(int Q){
        this.capacity=Q;
        this.customers=new LinkedList<GraphNode>();
        this.waitingTimes=new LinkedList<Double>();
        this.arrivalTimes=new LinkedList<Double>();
    }

    //copy constructor
    public Vehicle(Vehicle v){
        this.capacity=v.capacity;
        this.customers=new LinkedList<GraphNode>(v.customers);
        this.waitingTimes=new LinkedList<Double>(v.waitingTimes);
        this.arrivalTimes=new LinkedList<Double>(v.arrivalTimes);
    }

    //mutators

    public void setCapacity(int Q){
        this.capacity=Q;
    }

    public void addCustomer(GraphNode customer,Double waitingTime,Double arrivalTime){
        this.customers.add(customer);
        this.waitingTimes.add(waitingTime);
        this.arrivalTimes.add(arrivalTime);
        this.capacity=this.capacity-customer.getDemand();
    }

    public void addIthCustomer(GraphNode customer,int position,double waitingTime,double arrivalTime){
        this.customers.add(position,customer);
        this.waitingTimes.add(position, waitingTime);
        this.arrivalTimes.add(position, arrivalTime);
        this.capacity=this.capacity-customer.getDemand();
    }

    public void addIthCustomer(int position,GraphNode customer){
        this.customers.add(position,customer);
        this.waitingTimes.add(position, 0.0);
        this.arrivalTimes.add(position, 0.0);
        this.capacity=this.capacity-customer.getDemand();
    }

    public GraphNode removeCustomer(GraphNode customer){
        this.capacity=this.capacity+customer.getDemand();
        int i=this.customers.indexOf(customer);
        this.customers.remove(i);
        this.waitingTimes.remove(i);
        this.arrivalTimes.remove(i);
        return customer;
    }

    public void removeIthCustomer(int i){
        this.capacity=this.capacity+this.customers.get(i).getDemand();
        this.customers.remove(i);
        this.waitingTimes.remove(i);
        this.arrivalTimes.remove(i);
    }

    public void setCustomers(LinkedList<GraphNode> customers){
        this.customers=customers;
    }

    public void setWaitingTimes(LinkedList<Double> waitingTimes){
        this.waitingTimes=waitingTimes;
    }

    public void setArrivalTimes(LinkedList<Double> arrivalTimes){
        this.arrivalTimes=arrivalTimes;
    }

    //after a new customer insertion within the route, recalculate arrival and waiting times

    public void recalculateRoute(){
        //start from depot and while there are customers in the route compute their waiting and arrival times
        //based on their previous in the list

        for(int i=1;i<this.getCustomers().size();i++){
            GraphNode c=this.customers.get(i);
            Double curArrival;
            Double curWaiting;

            Double arrival=this.arrivalTimes.get(i-1);
            Double waiting=this.waitingTimes.get(i - 1);
            Double service=this.customers.get(i-1).getServiceTime();

            Double dist=c.getDistance(this.customers.get(i-1));

            curArrival=arrival+waiting+service+dist;

            if(c.getTimeWindow().getX()<=curArrival){//vehicle arrives and customer can already be serviced
                curWaiting=0.0;//no waiting here
            }
            else{//vehicle arrived too early
                curWaiting=c.getTimeWindow().getX()-curArrival;//it will wait for a while
            }
            this.arrivalTimes.set(i,curArrival);
            this.waitingTimes.set(i,curWaiting);

        }
    }

    //computes cost of route by summing nodes distances

    public Double getCost(){
        LinkedList<GraphNode> nodes=this.getCustomers();
        Double cost=0.0;
        for(int i=0;i<nodes.size();i++) {
            GraphNode n1;
            n1 = nodes.get(i);
            GraphNode n2;
            n2 = nodes.get((i + 1) % nodes.size());
            cost = cost + n1.getDistance(n2);
        }
        return cost;
    }

    //validates if vehicle can arrive on time at the depot
    //returns true iff vehicle can arrive in time
    public boolean checkDepot(){

        Double dist=this.getCost();//get route's cost

        GraphNode depot=this.customers.get(0);//initial node is always the depot

        Double limit=depot.getTimeWindow().getY();//the time window in which vehicle must return to depot

        return dist<=limit;

    }

    //validates if the vehicle can hold the cargo it is loaded with
    //returns true iff vehicle can hold its cargo
    public boolean checkCapacity(){
        return this.capacity>=0;
    }

    //validates if the route satisfies time window constraints
    //returns true iff time window constraints are satisfied
    public boolean checkOverlaps(){

        //for every customer
        for(int i=1;i<this.getCustomers().size();i++){
            GraphNode c=this.customers.get(i);
            Double curArrival,curWaiting;

            Double arrival=this.arrivalTimes.get(i - 1);
            Double waiting=this.waitingTimes.get(i-1);
            Double service=this.customers.get(i-1).getServiceTime();

            Double dist=c.getDistance(this.customers.get(i - 1));

            curArrival=arrival+waiting+service+dist;

            if(c.getTimeWindow().getX()<=curArrival){
                curWaiting=0.0;
            }
            else{
                curWaiting=c.getTimeWindow().getX()-curArrival;
            }

            if(curArrival+curWaiting>c.getTimeWindow().getY()&&(i-1!=0)){//if the customer cannot be serviced in time
                return false;
            }

            //System.out.println("TEST "+c.toString()+" "+curArrival+" "+curWaiting+" "+c.getTimeWindow().toString());
            //System.out.println(this.toString());

            GraphNode prevC=this.customers.get(i-1);

            Double s1,s2,e1,e2;
            s2=c.getTimeWindow().getX();
            e2=c.getTimeWindow().getY();
            s1=prevC.getTimeWindow().getX();
            e1=prevC.getTimeWindow().getY();

            if(!((s1<=s2&&s2<=e1&&e1<=e2)||(s1<=e1&&e1<=s2&&s2<=e2))&&(i-1!=0)){//if customers are not timely ordered
            //first condition describes the case where the time windows overlap but are in order
            //second condition describes the case where the time windows do not overlap AND are in order
                return false;
            }

        }

        return true;
    }

    //accessors

    public int getCapacity(){
        return this.capacity;
    }

    public LinkedList<GraphNode> getCustomers(){
        return this.customers;
    }

    public LinkedList<Double> getWaitingTimes(){
        return this.waitingTimes;
    }

    public LinkedList<Double> getArrivalTimes(){
        return this.arrivalTimes;
    }

    //output

    @Override

    public String toString(){
        return this.getCustomers().toString();

/*
        String result=new String();
        result=result+"[";
        for(GraphNode customers:this.getCustomers()){
            result=result+customers.toString()+",";
        }

        /*
        result=result+"]\n[";
        for(int i=1;i<this.getCustomers().size();i++){
            Double arrival=this.getArrivalTimes().get(i-1);
            Double waiting=this.getWaitingTimes().get(i-1);
            Double service=this.getCustomers().get(i - 1).getServiceTime();

            Double dist=this.getCustomers().get(i).getDistance(this.getCustomers().get(i - 1));

            Double curArrival=arrival+waiting+service+dist;

            Double curWaiting;

            if(this.getCustomers().get(i).getTimeWindow().getX()<=curArrival){//vehicle arrives and customer can already be serviced
                curWaiting=0.0;//no waiting here
            }
            else{//vehicle arrived too early
                curWaiting=this.getCustomers().get(i).getTimeWindow().getX()-curArrival;//it will wait for a while
            }

            result=result+"|"+curArrival+","+(curWaiting+curArrival+service)+","+dist+"|"+",";
        }


        result=result+"]"+" , len: "+this.getCustomers().size()+" , dist: "+this.getCost()+" , capacity: "+(200-this.getCapacity())+"\n";
        return result;
        */
    }
}
