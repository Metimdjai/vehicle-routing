package gr.uoa.di.theses.vr;

import java.util.LinkedList;

public class CandidateSolution {

    private LinkedList<Vehicle> improvedFleet;
    private Double diff;
    private String improvementMethod;
    private static int capacity;

    //constructors of CandidateSolution class

    CandidateSolution(LinkedList<Vehicle> fleet,int capacity,String improvementMethod){
        this.improvedFleet=new LinkedList<Vehicle>(fleet);
        this.diff=0.0;
        this.improvementMethod=improvementMethod;
        CandidateSolution.capacity=capacity;
    }

    //mutators

    public void setDiff(Double diff){
        this.diff=diff;
    }

    public void setCapacity(int capacity){
        CandidateSolution.capacity=capacity;
    }

    //accessors

    public LinkedList<Vehicle> getImprovedFleet(){
        return this.improvedFleet;
    }

    public Double getDiff(){
        return this.diff;
    }

    //output

    @Override

    public String toString(){
        String result;
        result="Candidate ("+this.improvementMethod+"):\n"+"\nRoutes: "+this.improvedFleet.size()+"\n\n";

        Double totalCost=0.0;

        for(Vehicle vehicle:this.improvedFleet){
            result=result+"[";
            for(GraphNode customers:vehicle.getCustomers()){
                if(vehicle.getCustomers().indexOf(customers)==0)
                    continue;
                result=result+customers.toString()+",";
            }
            result=result+vehicle.getCustomers().get(0).toString();
            result=result+"]"+" , len: "+vehicle.getCustomers().size()+" , dist: "+vehicle.getCost()+" , capacity: "+(CandidateSolution.capacity-vehicle.getCapacity())+"\n";
            totalCost=totalCost+vehicle.getCost();
        }

        result=result+"\nTotal cost: "+totalCost;

        return (result+"\nDifference from current solution: "+this.diff+"\n");
    }
}
