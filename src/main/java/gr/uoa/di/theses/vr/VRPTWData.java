package gr.uoa.di.theses.vr;

import edu.uci.ics.jung.graph.UndirectedSparseGraph;
import edu.uci.ics.jung.graph.util.Pair;

import java.io.*;
import java.util.Collection;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VRPTWData {

    //members of vrptwData class

    private UndirectedSparseGraph<GraphNode,GraphEdge> graph;
    private GraphNode depot;
    private LinkedList<Vehicle> fleet;
    private int capacity;
    private LinkedList<GraphNode> customers;

    //constructor of vrptwData class (reads data from file)

    VRPTWData(String path){
        this.graph=new UndirectedSparseGraph<GraphNode,GraphEdge>();
        this.customers=new LinkedList<GraphNode>();

        //read data
        int i,j;
        int q;
        FileInputStream fstream = null;
        try {
            fstream = new FileInputStream(path);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        assert fstream!=null;

        //use file
        DataInputStream in = new DataInputStream(fstream);
        //read input
        BufferedReader br = new BufferedReader(new InputStreamReader(in));

        try {
            String line2 = br.readLine();
            this.capacity=Integer.parseInt(line2);
            line2=br.readLine();
            int dim=Integer.parseInt(line2);
            for(i=0;i<=dim;i++){
                line2=br.readLine();

                String line=line2.replaceAll("\\s+"," ");

                Pattern p=Pattern.compile("([^ ]+) ([0-9]*\\.?[0-9]*) ([0-9]*\\.?[0-9]*) (\\d+) (\\d+) (\\d+) (\\d+)");
                Matcher m=p.matcher(line);
                if(!m.find()){
                    System.out.println("Pattern matching failed");
                }

                String name;
                Double x,y;
                Double start,end;
                Double serviceTime;

                name=m.group(1);
                x= Double.parseDouble(m.group(2));
                y= Double.parseDouble(m.group(3));
                q= Integer.parseInt(m.group(4));
                start= Double.parseDouble(m.group(5));
                end= Double.parseDouble(m.group(6));
                serviceTime= Double.parseDouble(m.group(7));
                GraphNode node=new GraphNode(name,x,y,start,end,q,serviceTime);//end column in solomon's benchmark is the duration of the time window

                this.graph.addVertex(node);

                this.customers.add(node);
                if(i==0){
                    this.depot=node;
                }
            }

            //insert n+1 node which is the depot (exists twice in order to include the depot twice in each path as "unique nodes"
        /*
            GraphNode node=new GraphNode(this.depot);
            node.setName(String.valueOf(i));

            this.graph.addVertex(node);
            //System.out.println(node.toString());

        */

        } catch (IOException e) {
            e.printStackTrace();
        }

        Collection<GraphNode> customers= this.graph.getVertices();

        j=0;

        for(GraphNode source:customers){
            for(GraphNode target:customers){
                //customer1!=customer2 to get both (i,j) and (j,i)
                //if(customer1!=customer2){
                if(source.getName().compareTo(target.getName())>0) {

                    Double distance=source.getDistance(target);

                    this.graph.addEdge(new GraphEdge(j++,distance),source,target);

                }
            }
        }

        this.fleet=new LinkedList<Vehicle>();
    }

    //mutators

    public void setCapacity(int capacity){
        this.capacity=capacity;
    }

    public void initialSolution(){
        Vehicle vehicle=new Vehicle(this.capacity);

        this.fleet.add(vehicle);

        LinkedList<GraphNode> unserviced=new LinkedList<GraphNode>(this.customers);
        LinkedList<GraphNode> serviced=new LinkedList<GraphNode>();

        unserviced.remove(this.depot);

        this.addFirstCustomer(vehicle, serviced, unserviced);

        while(!unserviced.isEmpty()) {//while there are customers to service
            //System.out.println(serviced.toString());
            //System.out.println(unserviced.toString());
            Double finalArrival = vehicle.getArrivalTimes().getLast();
            Double finalWaiting = vehicle.getWaitingTimes().getLast();
            Double finalService = vehicle.getCustomers().getLast().getServiceTime();
            Double finalDistance = this.depot.getDistance(vehicle.getCustomers().getLast());
            Double limit = this.depot.getTimeWindow().getY();

            //if added customer was a valid addition
            //(vehicle capacity holds and the vehicle manages to return to the depot in time)
            if ((vehicle.getCapacity() > 0) && (finalArrival + finalWaiting + finalService + finalDistance < limit)) {

                /***************************************************************************************
                 //initialize a list of cost "nodes" for each candidate. Each candidate will have nodes
                 //of possible routes with the difference of the cost of their addition against adding
                 //them to a new route
                 ***************************************************************************************/
                LinkedList<CandidateRoute> candidates = new LinkedList<CandidateRoute>();

                //for every unserviced candidate
                for (GraphNode candidate : unserviced) {

                    Double newRouteCost = this.depot.getDistance(candidate) + vehicle.getCost();

                    //for every possible insertion of above candidate
                    for (int i = 1; i <= vehicle.getCustomers().size(); i++) {
                        Vehicle tmpVehicle = new Vehicle(vehicle);
                        tmpVehicle.addIthCustomer(candidate, i, 0, 0);

                        Double cost = tmpVehicle.getCost();

                        tmpVehicle.recalculateRoute();

                        if (tmpVehicle.checkOverlaps()) {//if the insertion of the new customer in the route
                            //satisfies time window constraints
                            CandidateRoute newRoute = new CandidateRoute(newRouteCost - cost, tmpVehicle, candidate);
                            candidates.add(newRoute);
                        }

                    }

                }

                if (candidates.isEmpty()) {//if no feasible route could exist
                    //a new route must begin but since the last addition was valid (only a feasible new route could not exist)
                    //the last customer of the current route should NOT be removed
                    this.generateNewRoute(vehicle, serviced, unserviced, 0);
                } else {//at least one feasible route exists
                    boolean entered = false;
                    CandidateRoute replacement = null;

                    //find the best route
                    for (CandidateRoute cand : candidates) {
                        if (!entered) {
                            entered = true;
                            replacement = cand;
                            continue;
                        }
                        if (cand.getDiff() > replacement.getDiff()) {
                            replacement = cand;
                        }
                    }
                    //update vehicle's route

                    assert replacement!=null;

                    vehicle.setCapacity(replacement.getVehicle().getCapacity());
                    vehicle.setArrivalTimes(replacement.getVehicle().getArrivalTimes());
                    vehicle.setWaitingTimes(replacement.getVehicle().getWaitingTimes());
                    vehicle.setCustomers(replacement.getVehicle().getCustomers());

                    //since the customer was serviced
                    //remove them from unserviced set and add them to serviced set
                    serviced.add(replacement.getToService());
                    unserviced.remove(replacement.getToService());
                }
            } else {
                //the last customer caused the route to either miss depot closure or the truck cannot carry all goods
                //in this case, a new route must be generated AFTER reading the last customer to unserviced set
                this.generateNewRoute(vehicle, serviced, unserviced, 1);
            }
            vehicle = this.fleet.getLast();//always get the last vehicle while iterating (in case a new route was created)
        }
    }

    private void addFirstCustomer(Vehicle vehicle,LinkedList<GraphNode> serviced,LinkedList<GraphNode> unserviced){
        //since a new route starts, always add depot first

        vehicle.addCustomer(this.depot,0.0,0.0);

        //get depot's neighbours
        Collection<GraphEdge> neighbours=this.graph.getOutEdges(this.depot);

        boolean entered=false;
        GraphEdge maxEdge = null;

        for(GraphEdge edge:neighbours){//for every possible candidate
            Pair<GraphNode> endpoints=this.graph.getEndpoints(edge);

            GraphNode customer;

            if(endpoints.getFirst()==this.depot){
                customer=endpoints.getSecond();
            }
            else{
                customer=endpoints.getFirst();
            }

            if(!entered){//if maxEdge has not been initialized

                if(!serviced.contains(customer)){//initialize ONLY when an unserviced customer is found
                    entered = true;
                    maxEdge = edge;
                }
            }

            if(entered&&edge.getDistance()>=maxEdge.getDistance()){//if a more distant customer was found

                if(!serviced.contains(customer)) {//find max distance customer who is unserviced
                    maxEdge = edge;
                }
            }
        }

        Pair<GraphNode> endpoints=this.graph.getEndpoints(maxEdge);

        GraphNode customer;

        if(endpoints.getFirst()==this.depot){
            customer=endpoints.getSecond();
        }
        else{
            customer=endpoints.getFirst();
        }

        assert maxEdge!=null;

        if(customer.getTimeWindow().getX()<=maxEdge.getDistance())//vehicle reaches customer within time window,therefore starts service immediately
            vehicle.addCustomer(customer,0.0,maxEdge.getDistance());
        else{//vehicle reaches customer before time window,therefore it must wait
            vehicle.addCustomer(customer,customer.getTimeWindow().getX()-maxEdge.getDistance(),maxEdge.getDistance());
        }

        //customer will now get serviced
        serviced.add(customer);
        unserviced.remove(customer);
    }

    //starts a new route
    //depending on mode of calling, it will either remove the final customer from the route or do nothing.
    //Then, it will create a new route by adding the depot at the beginning and a first customer that is further from the depot

    private void generateNewRoute(Vehicle vehicle,LinkedList<GraphNode> serviced,LinkedList<GraphNode> unserviced,int mode){

        if(mode==1){//if called to remove last customer from route initially
            GraphNode customer = vehicle.removeCustomer(vehicle.getCustomers().getLast());

            //since the customer was not serviced after all, they are again added to the unserviced set
            serviced.remove(customer);
            unserviced.add(customer);
        }

        Vehicle vehicle2=new Vehicle(this.capacity);

        this.fleet.add(vehicle2);

        //add the furthest customer who has NOT been yet serviced in route
        this.addFirstCustomer(vehicle2, serviced, unserviced);
    }

    //accessors

    public UndirectedSparseGraph<GraphNode,GraphEdge> getGraph(){
        return this.graph;
    }

    public LinkedList<Vehicle> getFleet(){
        return this.fleet;
    }


    public int getCapacity(){
        return this.capacity;
    }

    //output

    @Override

    public String toString(){
        String result;
        result="Solution:\n"+"\nCapacity: "+this.getCapacity()+"\n"+"Routes: "+this.fleet.size()+"\n\n";

        Double totalCost=0.0;

        for(Vehicle vehicle:this.fleet){
            result=result+"[";
            for(GraphNode customers:vehicle.getCustomers()){
                if(vehicle.getCustomers().indexOf(customers)==0)
                    continue;
                result=result+customers.toString()+",";
            }
            result=result+vehicle.getCustomers().get(0).toString();
            /*
            result=result+"]\n[";
            for(int i=1;i<vehicle.getCustomers().size();i++){
                Double arrival=vehicle.getArrivalTimes().get(i-1);
                Double waiting=vehicle.getWaitingTimes().get(i-1);
                Double service=vehicle.getCustomers().get(i - 1).getServiceTime();

                Double dist=vehicle.getCustomers().get(i).getDistance(vehicle.getCustomers().get(i - 1));

                Double curArrival=arrival+waiting+service+dist;

                Double curWaiting;

                if(vehicle.getCustomers().get(i).getTimeWindow().getX()<=curArrival){//vehicle arrives and customer can already be serviced
                    curWaiting=0.0;//no waiting here
                }
                else{//vehicle arrived too early
                    curWaiting=vehicle.getCustomers().get(i).getTimeWindow().getX()-curArrival;//it will wait for a while
                }

                result=result+"|"+curArrival+","+(curWaiting+curArrival+service)+","+dist+"|"+",";
            }
               */

            result=result+"]"+" , len: "+vehicle.getCustomers().size()+" , dist: "+vehicle.getCost()+" , capacity: "+(this.getCapacity()-vehicle.getCapacity())+"\n";
            totalCost=totalCost+vehicle.getCost();
        }

        result=result+"\nTotal cost: "+totalCost+"\n\n";

        return result;
    }

}
