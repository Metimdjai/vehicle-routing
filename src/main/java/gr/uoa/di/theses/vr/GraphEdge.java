package gr.uoa.di.theses.vr;

public class GraphEdge {

    //members of graphEdge class

    private int id;
    private double distance;
    private int penalty;
    private Double updatedDistance;

    //constructor

    GraphEdge(int id,Double distance){
        this.id=id;
        this.distance=distance;
        this.penalty=0;
        this.updatedDistance=distance;
    }

    //mutators

    public void setDistance(int distance){
        this.distance=distance;
    }

    public void updateDistance(Double lambda){
        this.updatedDistance=this.distance+lambda*this.distance*this.penalty;
    }

    public void increasePenalty(){
        this.penalty++;
    }

    //accessors

    public Double getDistance(){
        return this.distance;
    }

    public int getPenalty(){
        return this.penalty;
    }

    //output

    @Override

    public String toString(){
        return ("("+this.id+","+this.updatedDistance+","+this.penalty+","+this.distance+")");
    }

}