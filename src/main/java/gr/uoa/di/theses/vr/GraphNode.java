package gr.uoa.di.theses.vr;

import edu.uci.ics.jung.graph.util.Pair;

public class GraphNode {

    //members of graphNode class

    private String name;
    private TimeWindow timeWindow;
    private int q;
    private Double serviceTime;
    private Point coordinates;

    //constructor

    public GraphNode(String name,Double x,Double y,Double start,Double end,int q,Double serviceTime){
        this.name=name;
        this.timeWindow=new TimeWindow(start,end);
        this.q=q;
        this.serviceTime=serviceTime;
        this.coordinates=new Point(x,y);
    }

    //accessors

    public String getName(){
        return this.name;
    }

    public TimeWindow getTimeWindow(){
        return this.timeWindow;
    }

    public int getDemand(){
        return this.q;
    }

    public Double getServiceTime(){
        return this.serviceTime;
    }

    public Pair<Double> getCoordinates(){
        return new Pair<Double>(this.coordinates.getX(),this.coordinates.getY());
    }

    public Double getDistance(GraphNode target){
        return Math.sqrt(Math.pow((this.coordinates.getX()-target.coordinates.getX()),2)+Math.pow((this.coordinates.getY()-target.coordinates.getY()),2));
        /*
        Pair<Double> source=this.getCoordinates();
        Pair<Double> target=targetNode.getCoordinates();

        //computes distance of two points on earth

        Double lat1,lon1,lat2,lon2;

        lat1=source.getFirst();
        lon1=source.getSecond();
        lat2=target.getFirst();
        lon2=target.getSecond();

        Double R = 6371.0; // km
        Double dLat = (lat2-lat1)*(Math.PI/180.0);
        Double dLon = (lon2-lon1)*(Math.PI/180.0);
        Double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(lat1*(Math.PI/180.0)) * Math.cos(lat2*(Math.PI/180.0)) *
                        Math.sin(dLon/2) * Math.sin(dLon/2);
        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        return R * c;
        */
    }

    //output

    @Override

    public String toString(){
        return (this.name);
    }
}
