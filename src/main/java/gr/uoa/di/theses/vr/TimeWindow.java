package gr.uoa.di.theses.vr;

public class TimeWindow {

    //members of point class

    private Double x;
    private Double y;

    //constructor

    TimeWindow(Double x,Double y){
        this.x=x;
        this.y=y;
    }

    //accessors

    public double getX(){
        return this.x;
    }

    public double getY(){
        return this.y;
    }

    //output

    @Override

    public String toString(){
        return ("("+this.x+","+this.y+")");
    }

}
