package gr.uoa.di.theses.vr;

import com.sun.istack.internal.Nullable;
import edu.uci.ics.jung.graph.UndirectedSparseGraph;
import edu.uci.ics.jung.graph.util.Pair;

import java.util.*;

public class GLS {

    private LinkedList<Vehicle> fleet;
    private LinkedList<Vehicle> candidateFleet;
    private Double lambda;
    private int capacity;
    private UndirectedSparseGraph<GraphNode,GraphEdge> graph;

    //constructors of GLS class

    public GLS(LinkedList<Vehicle> fleet,UndirectedSparseGraph<GraphNode,GraphEdge> graph,Double lambda,int capacity){
        this.fleet=new LinkedList<Vehicle>(fleet);
        this.candidateFleet=new LinkedList<Vehicle>(fleet);
        this.lambda=lambda;
        this.capacity=capacity;
        this.graph=graph;
    }

    public GLS(GLS gls){
        this.fleet=new LinkedList<Vehicle>(gls.fleet);
        this.candidateFleet=gls.candidateFleet;
        this.lambda=gls.lambda;
        this.capacity=gls.capacity;
    }

    //penalises vector
    public void penaliseVector(){

        LinkedList <Pair<GraphNode>> edges=new LinkedList<Pair<GraphNode>>();//get a list of all edges from graph used in current solution

        for(Vehicle v:this.candidateFleet){
            for(int i=0;i<v.getCustomers().size();i++){
                GraphNode v1,v2;
                v1=v.getCustomers().get(i);
                v2=v.getCustomers().get((i+1)%v.getCustomers().size());
                edges.add(new Pair<GraphNode>(v1,v2));//for every pair of customers in every route, add a new edge connecting the two
            }
        }

        boolean entered=false;
        Double maxCost=0.0;

        for(Pair<GraphNode> endpoints:edges){//find max cost to penalize among edges based on the formula cost=ci/(pi+1)
            GraphEdge edge=this.graph.findEdge(endpoints.getFirst(),endpoints.getSecond());
            if(edge==null)
                continue;

            Double cost=edge.getDistance();
            int penalty=edge.getPenalty();

            if(!entered){
                maxCost=cost/(penalty+1);
                entered=true;
            }
            if(cost/(penalty+1)>maxCost){
                maxCost=cost/(penalty+1);
            }

        }

        //for every edge
        for(Pair<GraphNode> endpoints:edges){
            GraphEdge edge=this.graph.findEdge(endpoints.getFirst(),endpoints.getSecond());
            if(edge==null)
                continue;

            Double cost=edge.getDistance();
            int penalty=edge.getPenalty();

            if(cost/(penalty+1)==maxCost){//if it is to be penalized
                edge.increasePenalty();//penalty number increases by 1
                edge.updateDistance(this.lambda);//and this edge's distance is updated
            }
        }
    }

    public Double getObjectiveCost(LinkedList<Vehicle> fleet){
        Double cost=0.0;
        LinkedList<Pair<GraphNode>> edges=new LinkedList<Pair<GraphNode>>();
        for(Vehicle v:fleet){
            for(int i=0;i<v.getCustomers().size();i++){
                GraphNode v1,v2;
                v1=v.getCustomers().get(i);
                v2=v.getCustomers().get((i+1)%v.getCustomers().size());
                edges.add(new Pair<GraphNode>(v1,v2));//for every pair of customers in every route, add a new edge connecting the two
            }
        }

        for(Pair<GraphNode> endpoints:edges){//find max cost to penalize among edges based on the formula cost=ci/(pi+1)
            GraphEdge edge=this.graph.findEdge(endpoints.getFirst(),endpoints.getSecond());
            if(edge!=null) {
                Double edgeCost = edge.getDistance();
                cost = cost + edgeCost;
            }
        }

        return cost;
    }

    //succeeds iff gls offers a better solution than this instance
    public boolean solutionImproved(){
        Double oldObjective,newObjective;

        oldObjective=this.getObjectiveCost(this.fleet);
        newObjective=this.getObjectiveCost(this.candidateFleet);

        return newObjective<oldObjective;
    }

    public String localSearch(){

        //get cost of current solution
        Double curCost=this.getObjectiveCost(this.fleet);
        //create list candidate solutions
        LinkedList<CandidateSolution> candidates=new LinkedList<CandidateSolution>();

        for(int i=0;i<this.fleet.size();i++){//for every pair of routes
            for(int j=0;j<this.fleet.size();j++){
                if(i!=j){
                    Vehicle v1=new Vehicle(this.fleet.get(i));
                    Vehicle v2=new Vehicle(this.fleet.get(j));

                    Pair<Vehicle> candRelocate=this.relocate(v1, v2);//obtain an improved solution by relocate operator
                    Pair<Vehicle> candRelocateNew=this.relocateNew(v1);//obtain an improved solution by relocate operator to new route
                    Pair<Vehicle> candExchange = this.exchange(v1, v2);//obtain an improved solution by exchange operator
                    Pair<Vehicle> candCross = this.cross(v1, v2);//obtain an improved solution by cross operator
                    Pair<Vehicle> candCrossAppend=this.crossAppend(v1, v2);//obtain an improved solution by appending R2 to R1
                    Pair<Vehicle> candCrossSplit=this.crossSplit(v1, v2);//obtain an improved solution by splitting R1 in R1' and R2

                    LinkedList<Vehicle> relocateFleet,relocateNewFleet,exchangeFleet,crossFleet,crossAppendFleet,crossSplitFleet;
                    Double cost1,cost2,cost3,cost4,cost5,cost6;


                    if(candRelocate!=null){//if a valid relocation operator exists
                        relocateFleet=new LinkedList<Vehicle>(this.fleet);//generate new fleet
                        relocateFleet.set(i,candRelocate.getFirst());//replace the old routes with the improved ones
                        relocateFleet.set(j,candRelocate.getSecond());
                        CandidateSolution cand1=new CandidateSolution(relocateFleet,this.capacity,"RELOCATE");//new candidate solution
                        cost1=this.getObjectiveCost(relocateFleet);//get cost of objective function
                        cand1.setDiff(curCost-cost1);//set diff
                        candidates.add(cand1);//add to candidates
                    }

                    if(candRelocateNew!=null){//if a valid relocation operator exists
                        relocateNewFleet=new LinkedList<Vehicle>(this.fleet);//generate new fleet
                        relocateNewFleet.set(i,candRelocateNew.getFirst());//replace the old routes with the improved ones
                        relocateNewFleet.add(candRelocateNew.getSecond());
                        CandidateSolution cand2=new CandidateSolution(relocateNewFleet,this.capacity,"RELOCATE-NEW");//new candidate solution
                        cost2=this.getObjectiveCost(relocateNewFleet);//get cost of objective function
                        cand2.setDiff(curCost-cost2);//set diff
                        candidates.add(cand2);//add to candidates
                    }

                    if(candExchange!=null){//if a valid exchange operator exists
                        exchangeFleet=new LinkedList<Vehicle>(this.fleet);//generate new fleet
                        exchangeFleet.set(i, candExchange.getFirst());//replace the old routes with the improved ones
                        exchangeFleet.set(j, candExchange.getSecond());
                        CandidateSolution cand3=new CandidateSolution(exchangeFleet,this.capacity,"EXCHANGE");//new candidate solution
                        cost3=this.getObjectiveCost(exchangeFleet);//get cost of objective function
                        cand3.setDiff(curCost-cost3);//set diff
                        candidates.add(cand3);//add to candidates
                    }

                    if(candCross!=null){//if a valid cross operator exists
                        crossFleet=new LinkedList<Vehicle>(this.fleet);//generate new fleet
                        crossFleet.set(i,candCross.getFirst());//replace old routes with the improved ones
                        crossFleet.set(j,candCross.getSecond());
                        CandidateSolution cand4=new CandidateSolution(crossFleet,this.capacity,"CROSS");//new candidate solution
                        cost4=this.getObjectiveCost(crossFleet);//get cost of objective function
                        cand4.setDiff(curCost-cost4);//set diff
                        candidates.add(cand4);//add to candidates
                    }

                    if(candCrossAppend!=null){//if a valid cross-append operator that appends R2 to R1 exist
                        crossAppendFleet=new LinkedList<Vehicle>(this.fleet);//generate new fleet
                        crossAppendFleet.set(i,candCrossAppend.getFirst());//replace old routes with the improved ones
                        crossAppendFleet.set(j,candCrossAppend.getSecond());
                        CandidateSolution cand5=new CandidateSolution(crossAppendFleet,this.capacity,"CROSS-APPEND");//new candidate solution
                        cost5=this.getObjectiveCost(crossAppendFleet);//get cost of objective function
                        cand5.setDiff(curCost-cost5);//set diff
                        candidates.add(cand5);//add to candidates
                    }

                    if(candCrossSplit!=null){//if a valid cross-split operator that appends R2 to R1 exist
                        crossSplitFleet=new LinkedList<Vehicle>(this.fleet);//generate new fleet
                        crossSplitFleet.set(i,candCrossSplit.getFirst());//replace old routes with the improved ones
                        crossSplitFleet.set(j,candCrossSplit.getSecond());
                        CandidateSolution cand6=new CandidateSolution(crossSplitFleet,this.capacity,"CROSS-SPLIT");//new candidate solution
                        cost6=this.getObjectiveCost(crossSplitFleet);//get cost of objective function
                        cand6.setDiff(curCost-cost6);//set diff
                        candidates.add(cand6);//add to candidates
                    }
                }
            }
        }

        //find best 2-opt improvement from all routes

        for(int i=0;i<this.fleet.size();i++){
            LinkedList<Vehicle> opt2Fleet=new LinkedList<Vehicle>(this.fleet);

            Vehicle v=new Vehicle(this.fleet.get(i));
            Vehicle improvedOpt2=this.opt2(v);//perform 2opt improvement

            if(improvedOpt2!=null){
                opt2Fleet.set(i, improvedOpt2);
                CandidateSolution cand = new CandidateSolution(opt2Fleet, this.capacity, "2-OPT");
                cand.setDiff(curCost - this.getObjectiveCost(opt2Fleet));
                candidates.add(cand);
            }
        }


        //ALTERNATE IMPLEMENTATION (OLD): APPLY 2-OPT ON ALL ROUTES AND CONSIDER THIS AS AN ONE-STEP IMPROVEMENT

        /*

        //since 2-opt is applied to a single route, all 2-opts per local searched can be applied SIMULTANEOUSLY
        //and the cost diff from the current solution is calculated afterwards
        boolean improvedOnce=false;

        CandidateSolution candidate=new CandidateSolution(this.fleet,this.capacity,"2-OPT");
        for(int i=0;i<this.fleet.size();i++){//for every route
            Vehicle v=new Vehicle(this.fleet.get(i));
            Vehicle improvedOpt2=this.opt2(v);//perform 2opt improvement

            if(improvedOpt2!=null){//if an actual improvement could be found on this route
                candidate.getImprovedFleet().set(i, improvedOpt2);//and replace old route with a new one
                improvedOnce=true;
            }//otherwise, old route remains in the improved solution
        }
        candidate.setDiff(curCost - this.getObjectiveCost(candidate.getImprovedFleet()));

        if(improvedOnce)
            candidates.add(candidate);

        //find best improvement among all candidates

        */

        //END OF ALTERNATE IMPLEMENTATION
        CandidateSolution max=null;

        boolean entered=false;

        for(CandidateSolution cand:candidates){
            //System.out.println(cand.toString());
            if(!entered&&cand.getDiff()>0){
                entered=true;
                max=cand;
            }
            if(entered&&cand.getDiff()>max.getDiff()){
                max=cand;
            }
        }

        //set the fleet accordingly

        String res="";

        res+="CHANGE TO DO IN THIS STEP OF GLS\n\n";

        if(max!=null){
            res+=max.toString()+"\n";

            LinkedList<Vehicle> finalFleet=new LinkedList<Vehicle>();//generate new linked list

            for(Vehicle v:max.getImprovedFleet()){
                if(v.getCustomers().size()>1){//for every route in fleet that has at least one customer to service
                    finalFleet.add(v);//add route in final fleet
                }
            }

            this.setCandidateFleet(finalFleet);//set new candidateSolution as the final fleet
        }

        res+="END OF CHANGE TO DO IN THIS STEP OF GLS\n\n";

        return res;
    }

    //local search operators

    private Vehicle opt2swap(Vehicle vehicle,int i,int j){
        Vehicle v=new Vehicle(this.capacity);

        for(int c=0;c<i;c++){//take route[0] to route[i-1] and add them in order to new route
            v.addCustomer(vehicle.getCustomers().get(c),0.0,0.0);
        }

        for(int c=j;c>=i;c--){//take route[i] to route[j] and add them in reverse order to new route
            v.addCustomer(vehicle.getCustomers().get(c), 0.0, 0.0);
        }

        for(int c=j+1;c<vehicle.getCustomers().size();c++){//take route[j+1] to end of old route and add them in order to new route
            v.addCustomer(vehicle.getCustomers().get(c),0.0,0.0);
        }

        v.recalculateRoute();//recalculate arrival and waiting times

        return v;
    }

    //2-opt algorithm for improving routes that reverses the part of the path between i and j

    @Nullable
    private Vehicle opt2(Vehicle vehicle){
        Vehicle v=new Vehicle(vehicle);

        boolean improvement=true;

        boolean improvedOnce=false;//true iff at least one improvement was done

        while(improvement){
            boolean breakCond=false;
            improvement=false;
            LinkedList<Vehicle> testFleet=new LinkedList<Vehicle>();
            testFleet.add(v);

            Double best=this.getObjectiveCost(testFleet);

            for(int i=1;i<v.getCustomers().size()-1;i++){//for every customer in route R1 (excluding depot)
                for(int j=i+1;j<v.getCustomers().size();j++){//for every customer in route R2 (excluding depot)
                    Vehicle newRoute=this.opt2swap(v,i,j);

                    LinkedList<Vehicle> testCurFleet=new LinkedList<Vehicle>();
                    testCurFleet.add(newRoute);
                    Double cur=this.getObjectiveCost(testCurFleet);

                    if(cur<best&&v.checkCapacity()&&v.checkDepot()&&v.checkOverlaps()){//if improved and newRoute satisfies problem constraints
                        v=newRoute;//set route as the new improvement
                        improvedOnce=true;
                        breakCond=true;
                        break;
                    }
                }

                if(breakCond)
                    break;
            }
        }

        return improvedOnce?v:null;
    }

    //relocate algorithm for improving routes

    public Pair<Vehicle> relocate(Vehicle v1,Vehicle v2){

        if(v1.getCustomers().size()==1) {//if R1 is empty, no relocate is possible
            return null;
        }

        if(v2.getCustomers().size()==1){//if R2 is empty, one node from R1 can be added to it

            LinkedList<Vehicle> best=new LinkedList<Vehicle>();
            best.add(v1);
            best.add(v2);

            Double bestCost=this.getObjectiveCost(best);

            Pair<Vehicle> cand=null;

            //Try relocating each note from R1 to R2 and keep the best cost solution (if one exists)

            for(int i=1;i<v1.getCustomers().size();i++){
                Vehicle V1=new Vehicle(v1);
                Vehicle V2=new Vehicle(v2);
                GraphNode n=V1.getCustomers().get(i);
                V1.removeIthCustomer(i);
                V2.addCustomer(n,0.0,0.0);

                LinkedList<Vehicle> cur=new LinkedList<Vehicle>();
                cur.add(V1);
                cur.add(V2);

                Double curCost=this.getObjectiveCost(cur);

                if(curCost<bestCost){
                    cand=new Pair<Vehicle>(V1,V2);
                    bestCost=curCost;
                }
            }

            return cand;

        }

        //save routes R1 and R2 in case the operation violates time window, capacity or depot constraints

        Vehicle V1=new Vehicle(v1);
        Vehicle V2=new Vehicle(v2);

        GraphNode n1,n2;
        n1=n2=null;
        double minDist=0;
        GraphNode tmpn1,tmpn2;

        //get the pair of nodes (n1,n2) from R1 and R2 that have the smallest distance

        for(int i=1;i<V1.getCustomers().size();i++){
            tmpn1=V1.getCustomers().get(i);

            //try insertions until next-to-last node in R2 since n1 can be inserted at the end of the route in the worst scenario
            for(int j=1;j<V2.getCustomers().size();j++){

                tmpn2=V2.getCustomers().get(j);
                double dist=tmpn1.getDistance(tmpn2);

                if(i==1&&j==1){
                    minDist=dist;
                    n1=tmpn1;
                    n2=tmpn2;
                }

                if(dist<minDist){
                    minDist=dist;
                    n1=tmpn1;
                    n2=tmpn2;
                }
            }
        }

        Vehicle V3=new Vehicle(V1);
        Vehicle V4=new Vehicle(V2);

        //add n1 in R2 before n2

        int index1=V1.getCustomers().indexOf(n1);

        int index2=v2.getCustomers().indexOf(n2);

        V2.addIthCustomer(index2, n1);

        V1.removeIthCustomer(index1);

        //both routes need arrivals and waitings recalculation

        V1.recalculateRoute();
        V2.recalculateRoute();


        //add n1 in R2 after n2
        V4.addIthCustomer(index2 + 1, n1);
        V3.removeIthCustomer(index1);

        V3.recalculateRoute();
        V4.recalculateRoute();

        Pair <Vehicle> cand1,cand2;

        cand1=null;
        cand2=null;

        //if at least one route does not satisfy depot,time window and capacity constraints (case where n1 from R1 is inserted before n2 from R2)

        if(V1.checkOverlaps()&&V1.checkCapacity()&&V1.checkDepot()&&
                V2.checkOverlaps()&&V2.checkCapacity()&&V2.checkDepot()){

            cand1=new Pair<Vehicle>(V1,V2);
        }

        //if this route does not satisfy constraints either, no solution could obtained (case where n1 from R1 is inserted after n2 from R2)

        if(V3.checkOverlaps()&&V3.checkCapacity()&&V3.checkDepot()&&
                V4.checkOverlaps()&&V4.checkCapacity()&&V4.checkDepot()){

            cand2=new Pair<Vehicle>(V3,V4);
        }

        //return best solution for this step (or null if none could be obtained)

        if(cand1==null){
            return cand2;
        }

        if(cand2==null){
            return cand1;
        }

        LinkedList <Vehicle> o1,o2;

        o1=new LinkedList<Vehicle>();
        o1.add(V1);
        o1.add(V2);

        o2=new LinkedList<Vehicle>();
        o2.add(V3);
        o2.add(V4);

        if(this.getObjectiveCost(o1)<this.getObjectiveCost(o2)){
            return cand1;
        }
        else{
            return cand2;
        }

    }

    @Nullable
    private Pair<Vehicle> relocateNew(Vehicle v){

        LinkedList<Pair<Vehicle>> candidates=new LinkedList<Pair<Vehicle>>();

        for(int i=1;i<v.getCustomers().size();i++){
            Vehicle V=new Vehicle(v);
            Vehicle Vnew=new Vehicle(this.capacity);

            GraphNode n=V.getCustomers().get(i);
            GraphNode depot=V.getCustomers().get(0);

            V.removeIthCustomer(i);

            Vnew.addCustomer(depot,0.0,0.0);
            Vnew.addCustomer(n,0.0,0.0);

            V.recalculateRoute();
            Vnew.recalculateRoute();

            if(V.checkOverlaps()&&V.checkDepot()&&V.checkCapacity()&&Vnew.checkOverlaps()&&Vnew.checkDepot()&&Vnew.checkCapacity()){
                candidates.add(new Pair<Vehicle>(V,Vnew));
            }
        }

        if(candidates.isEmpty())
            return null;

        boolean entered=false;
        Double bestCost=0.0;

        Pair<Vehicle> improvement=null;

        for(Pair<Vehicle> candidate:candidates){
            Vehicle V1=candidate.getFirst();
            Vehicle V2=candidate.getSecond();

            LinkedList<Vehicle> cand=new LinkedList<Vehicle>();
            cand.add(V1);
            cand.add(V2);

            Double curCost=this.getObjectiveCost(cand);

            if(!entered){
                entered=true;
                bestCost=curCost;
                improvement=candidate;
            }

            if(curCost>bestCost){
                bestCost=curCost;
                improvement=candidate;
            }
        }

        return improvement;
    }

    @Nullable
    private Pair<Vehicle> exchange(Vehicle v1,Vehicle v2){
        if(v1.getCustomers().size()==1||v2.getCustomers().size()==1)//if at least one route is empty, no exchange is possible
            return null;

        //save routes R1 and R2 in case the operation violates time window, capacity or depot constraints

        Vehicle V1=new Vehicle(v1);
        Vehicle V2=new Vehicle(v2);

        GraphNode n1,n2;
        n1=n2=null;
        double minDist=0;
        GraphNode tmpn1,tmpn2;

        //get the pair of nodes (n1,n2) from R1 and R2 that have the smallest distance

        for(int i=1;i<V1.getCustomers().size();i++){
            tmpn1=V1.getCustomers().get(i);

            //try insertions until next-to-last node in R2 since n1 can be inserted at the end of the route in the worst scenario
            for(int j=1;j<V2.getCustomers().size();j++){

                tmpn2=V2.getCustomers().get(j);
                double dist=tmpn1.getDistance(tmpn2);

                if(i==1&&j==1){
                    minDist=dist;
                    n1=tmpn1;
                    n2=tmpn2;
                }

                if(dist<minDist){
                    minDist=dist;
                    n1=tmpn1;
                    n2=tmpn2;
                }
            }
        }

        int index1=V1.getCustomers().indexOf(n1);
        int index2=v2.getCustomers().indexOf(n2);

        assert n1!=null;

        int capacityn1=n1.getDemand();
        int capacityn2=n2.getDemand();

        V1.getCustomers().set(index1,n2);
        V2.getCustomers().set(index2,n1);

        V1.setCapacity(V1.getCapacity() + capacityn1 - capacityn2);
        V2.setCapacity(V2.getCapacity() + capacityn2 - capacityn1);

        V1.recalculateRoute();
        V2.recalculateRoute();

        if(V1.checkOverlaps()&&V1.checkCapacity()&&V1.checkDepot()&&
                V2.checkOverlaps()&&V2.checkCapacity()&&V2.checkDepot()){

            return new Pair<Vehicle>(V1,V2);
        }

        return null;
    }

    @Nullable
    private Pair<Vehicle> cross(Vehicle v1,Vehicle v2){
        if(v1.getCustomers().size()==1||v2.getCustomers().size()==1)//if at least one route is empty, no cross is possible
            return null;

        LinkedList<Vehicle> oldCand=new LinkedList<Vehicle>();
        oldCand.add(v1);
        oldCand.add(v2);

        Double oldCost=this.getObjectiveCost(oldCand);

        Vehicle V1,V2;

        LinkedList<Pair<Vehicle>> candidates=new LinkedList<Pair<Vehicle>>();

        for(int i=2;i<v1.getCustomers().size();i++){

            for(int j=2;j<v2.getCustomers().size();j++){
                LinkedList<GraphNode> sub1=new LinkedList<GraphNode>(v1.getCustomers().subList(i,v1.getCustomers().size()));
                LinkedList<GraphNode> sub2=new LinkedList<GraphNode>(v2.getCustomers().subList(j,v2.getCustomers().size()));

                V1=new Vehicle(v1);
                V2=new Vehicle(v2);

                for(int k=i;k<v1.getCustomers().size();k++){
                    V1.removeIthCustomer(i);
                }

                for(int k=j;k<v2.getCustomers().size();k++){
                    V2.removeIthCustomer(j);
                }

                int size1=sub2.size();
                int size2=sub1.size();
                for(int k=i;k<size1+i;k++){
                    V1.addIthCustomer(k,sub2.remove(sub2.indexOf(sub2.getLast())));
                }

                for(int k=j;k<size2+j;k++){
                    V2.addIthCustomer(k,sub1.remove(sub1.indexOf(sub1.getLast())));
                }

                if(V1.checkOverlaps()&&V1.checkCapacity()&V1.checkDepot()
                        &&V2.checkOverlaps()&&V2.checkCapacity()&&V2.checkDepot()){
                    candidates.add(new Pair<Vehicle>(V1, V2));
                }

            }
        }

        if(candidates.isEmpty())
            return null;

        boolean entered=false;
        Double bestCost=oldCost;

        Pair<Vehicle> improvement=null;

        for(Pair<Vehicle> candidate:candidates){
            V1=candidate.getFirst();
            V2=candidate.getSecond();

            LinkedList<Vehicle> cand=new LinkedList<Vehicle>();
            cand.add(V1);
            cand.add(V2);

            Double curCost=this.getObjectiveCost(cand);

            if(!entered){
                entered=true;
                bestCost=curCost;
                improvement=candidate;
            }

            if(curCost>bestCost){
                bestCost=curCost;
                improvement=candidate;
            }
        }

        return improvement;
    }

    @Nullable
    private Pair<Vehicle> crossAppend(Vehicle v1,Vehicle v2){
        Vehicle V1=new Vehicle(v1);
        Vehicle V2=new Vehicle(v2);

        //get all customers in R2 except for depot
        LinkedList<GraphNode> customers=new LinkedList<GraphNode>(V2.getCustomers().subList(1,V2.getCustomers().size()));

        //remove them from R2
        for(int i=1;i<v2.getCustomers().size();i++){
            V2.removeIthCustomer(1);
        }

        //append them to R1
        for(GraphNode customer:customers){
            V1.addCustomer(customer,0.0,0.0);
        }

        //recalculate routes costs
        V1.recalculateRoute();
        V2.recalculateRoute();

        //if constraints are satisfied
        if(V1.checkOverlaps()&&V1.checkCapacity()&V1.checkDepot()
                &&V2.checkOverlaps()&&V2.checkCapacity()&&V2.checkDepot()){
            return new Pair<Vehicle>(V1,V2);//return solution
        }
        return null;//else null is returned
    }

    @Nullable
    private Pair<Vehicle> crossSplit(Vehicle v1,Vehicle v2){

        if(v2.getCustomers().size()!=1)
            return null;

        LinkedList<Pair<Vehicle>> candidates=new LinkedList<Pair<Vehicle>>();

        for(int i=2;i<v1.getCustomers().size();i++){

            Vehicle V1=new Vehicle(v1);
            Vehicle V2=new Vehicle(v2);

            LinkedList<GraphNode> relocationPart=new LinkedList<GraphNode>(V1.getCustomers().subList(i,v1.getCustomers().indexOf(V1.getCustomers().getLast())));

            for(GraphNode customer:relocationPart){
                V2.addCustomer(customer,0.0,0.0);
                V1.removeCustomer(customer);
            }

            V1.recalculateRoute();
            V2.recalculateRoute();

            if(V1.checkOverlaps()&&V1.checkDepot()&&V1.checkCapacity()&&V2.checkOverlaps()&&V2.checkDepot()&&V2.checkCapacity()){
                candidates.add(new Pair<Vehicle>(V1,V2));
            }
        }

        if(candidates.isEmpty())
            return null;

        boolean entered=false;
        Double bestCost=0.0;

        Pair<Vehicle> improvement=null;

        for(Pair<Vehicle> candidate:candidates){
            Vehicle V1=candidate.getFirst();
            Vehicle V2=candidate.getSecond();

            LinkedList<Vehicle> cand=new LinkedList<Vehicle>();
            cand.add(V1);
            cand.add(V2);

            Double curCost=this.getObjectiveCost(cand);

            if(!entered){
                entered=true;
                bestCost=curCost;
                improvement=candidate;
            }

            if(curCost>bestCost){
                bestCost=curCost;
                improvement=candidate;
            }
        }

        return improvement;
    }

    //mutators

    public void setCapacity(int capacity){
        this.capacity=capacity;
    }

    public void setSolution(){
        this.fleet=this.candidateFleet;
    }

    public void setCandidateFleet(LinkedList<Vehicle>fleet){
        this.candidateFleet=fleet;
        /*for(Vehicle v:this.candidateFleet){
            System.out.println(v.toString());
        }*/
    }

    //accessors

    public int getCapacity(){
        return this.capacity;
    }

    public LinkedList<Vehicle> getFleet(){
        return this.fleet;
    }

    public boolean equalSolution(LinkedList<Vehicle> cand){
        return this.getObjectiveCost(cand).equals(this.getObjectiveCost(this.fleet));
    }

    //output

    @Override

    public String toString(){
        String result;
        result="Solution:\n"+"\nCapacity: "+this.getCapacity()+"\n"+"Routes: "+this.fleet.size()+"\n\n";

        Double totalCost=0.0;

        for(Vehicle vehicle:this.fleet){
            result=result+"[";
            for(GraphNode customers:vehicle.getCustomers()){
                if(vehicle.getCustomers().indexOf(customers)==0)
                    continue;
                result=result+customers.toString()+",";
            }
            result=result+vehicle.getCustomers().get(0).toString();
            /*

            result=result+"]\n[";
            for(int i=1;i<vehicle.getCustomers().size();i++){
                Double arrival=vehicle.getArrivalTimes().get(i-1);
                Double waiting=vehicle.getWaitingTimes().get(i-1);
                Double service=vehicle.getCustomers().get(i - 1).getServiceTime();

                Double dist=vehicle.getCustomers().get(i).getDistance(vehicle.getCustomers().get(i - 1));

                Double curArrival=arrival+waiting+service+dist;

                Double curWaiting;

                if(vehicle.getCustomers().get(i).getTimeWindow().getX()<=curArrival){//vehicle arrives and customer can already be serviced
                    curWaiting=0.0;//no waiting here
                }
                else{//vehicle arrived too early
                    curWaiting=vehicle.getCustomers().get(i).getTimeWindow().getX()-curArrival;//it will wait for a while
                }

                result=result+"|"+curArrival+","+(curWaiting+curArrival+service)+","+dist+"|"+",";
            }
            */

            result=result+"]"+" , len: "+(vehicle.getCustomers().size()-1)+" , dist: "+vehicle.getCost()+" , capacity: "+(this.getCapacity()-vehicle.getCapacity())+"\n";
            totalCost=totalCost+vehicle.getCost();
        }

        result=result+"\nTotal cost: "+totalCost+"\n\n";

        return result;
    }
}
