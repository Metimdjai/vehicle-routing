package gr.uoa.di.theses.vr;

public class CandidateRoute {

    //members of CandidateRoute class

    private Double diff;
    private Vehicle route;
    private GraphNode toService;

    CandidateRoute(Double diff,Vehicle route,GraphNode toService){
        this.diff=diff;
        this.route=route;
        this.toService=toService;
    }

    //mutators

    public void setVehicle(Vehicle route){
        this.route=new Vehicle(route);
    }

    //accessors

    public Double getDiff(){
        return this.diff;
    }

    public Vehicle getVehicle(){
        return this.route;
    }

    public GraphNode getToService(){
        return this.toService;
    }

    //output

    @Override

    public String toString(){
        return ("("+this.diff+","+this.route.toString()+","+this.toService.toString()+")");
    }

}
